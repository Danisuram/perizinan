<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
        
class Detail_bangunan extends MY_Controller {

function __construct()
 	{
 		parent::__construct();
 		$this->load->model('Mdetail_bangunan');
 		$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
 		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
 		$this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
 		$this->output->set_header('Pragma: no-cache');
 	}

	public function index()
	{

		$data['group_menu'] = 'hr';
        $data['content']    = 'detail_bangunan';
        $data['id_pengajuan'] = $this->input->GET('id');
		$data['id_bangunan'] = $this->input->GET('idbngn');
        $data['Bangunan'] = $this->Mdetail_bangunan->getBangunan($data['id_bangunan'] );
        $data['Tower'] = $this->Mdetail_bangunan->getTower($data['id_bangunan']);
        $data['IPTB'] = $this->Mdetail_bangunan->getDataIPTB();
        $data['SumurResapan'] = $this->Mdetail_bangunan->getDataSumurResapan();
        $data['Tanah'] = $this->Mdetail_bangunan->getDataTanah();
        $data['SIPPT'] = $this->Mdetail_bangunan->getDataSIPPT();
        $data['IMB'] = $this->Mdetail_bangunan->getDataIMB();
        $data['SLF'] = $this->Mdetail_bangunan->getDataSLF();

		
		$this->load->view('base/index', $data);
	}
	
}