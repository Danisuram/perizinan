<?php

class Mdetail_bangunan extends CI_Model{
    function getBangunan($id_bangunan){
        $this->db->select('NM_BANGUNAN, LOKASI_BANGUNAN, PROVINSI, KOTA, KECAMATAN, KELURAHAN, 
                         SUBLOCK, ZONASI, FUNGSI_BANGUNAN, PENGGUNA_BANGUNAN');
        $this->db->where('ID_PEMILIK', $this->session->userdata('id'));
        $this->db->where('ID_BANGUNAN', $id_bangunan);
        $query=$this->db->get('SYSTEM.BANGUNAN');
        if ($query->num_rows())
        {
            return $query->result_array();
        }
        else
        {
            return FALSE;
        }
    }

    function getTower($id_bangunan){
        $this->db->select('NAMA_TOWER, TINGGI_BANGUNAN, JML_MEIZANIN, JML_BASEMENT, L_LANTAI_DASAR, L_BANGUNAN, L_TAPAK_BASEMENT, TINGKAT_HUNIAN');
        $this->db->where('ID_BANGUNAN', $id_bangunan);
        $query=$this->db->get('SYSTEM.TEKNIS_BANGUNAN_TOWER');
        if ($query->num_rows())
        {
            return $query->result_array();
        }
        else
        {
            return FALSE;
        }
    }

    function getDataIPTB(){
        $this->db->select('NM_PERUSAHAAN, NM_IPTB, NO_IPTB, JNS_IPTB, NO_KTP, NO_NPWP, 
                        ALAMAT_KTP, ALAMAT_SKRG, NO_TELP, EMAIL');
        $this->db->where('ID_IPTB = 1');
        $query=$this->db->get('SYSTEM.IPTB');
        if ($query->num_rows())
        {
            return $query->result_array();
        }
        else
        {
            return FALSE;
        }

        
    }

    function getDataSumurResapan(){
        $this->db->select('JML_SUMUR_RESAPAN_BERFUNGSI, VLM_SUMUR_RESAPAN_BERFUNGSI');
        $query=$this->db->get('SYSTEM.TEKNIS_BANGUNAN');
        if ($query->num_rows())
            {
                return $query->result_array();
            }
            else
            {
                return FALSE;
            }
    }

    function getDataIMB(){
        $this->db->select('NO_IMB, TGL_IMB, L_BANGUNAN');
        $query=$this->db->get('SYSTEM.IMB');
        if ($query->num_rows())
            {
                return $query->result_array();
            }
            else
            {
                return FALSE;
            }
    }

    function getDataTanah(){
        $this->db->select('PAJAK_BANGUNAN.ID_PAJAK_BANGUNAN, PAJAK_BANGUNAN.NOP, PAJAK_BANGUNAN.LUAS_BUMI, PAJAK_BANGUNAN.LUAS_BANGUNAN, 
            SERT_BANGUNAN.ID_SERT_BANGUNAN, SERTIFIKAT_TANAH.NO_SERTIFIKAT, SERTIFIKAT_TANAH.LUAS_TANAH');
        $this->db->from('SYSTEM.SERTIFIKAT_TANAH');
        $this->db->join('SYSTEM.PAJAK_BANGUNAN','SERTIFIKAT_TANAH.ID_PEMILIK = PAJAK_BANGUNAN.ID_PEMILIK','inner');
        $this->db->join('SYSTEM.SERT_BANGUNAN','SERTIFIKAT_TANAH.ID_SERTIFIKAT = SERT_BANGUNAN.ID_SERTIFIKAT','inner');
        $query=$this->db->get();
        if ($query->num_rows())
            {
                return $query->result_array();
            }
            else
            {
                return FALSE;
            }
    }

    function getDataSIPPT(){
        $this->db->select('NO_SIPPT, TGL_SIPPT, LUAS_LAHAN');
        $query=$this->db->get('SYSTEM.SIPPT');
        if ($query->num_rows())
            {
                return $query->result_array();
            }
            else
            {
                return FALSE;
            }
    }

    function getDataSLF(){
        $this->db->select('NO_KRK, TGL_KRK');
        $query=$this->db->get('SYSTEM.KRK');
        if ($query->num_rows())
            {
                return $query->result_array();
            }
            else
            {
                return FALSE;
            }
    }

    

}