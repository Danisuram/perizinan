<style>

.panel.with-nav-tabs .panel-heading{
    padding: 5px 5px 0 5px;
}
.panel.with-nav-tabs .nav-tabs{
	border-bottom: none;
}
.panel.with-nav-tabs .nav-justified{
	margin-bottom: -1px;
}
/********************************************************************/
/*** PANEL DEFAULT ***/
.with-nav-tabs.panel-default .nav-tabs > li > a,
.with-nav-tabs.panel-default .nav-tabs > li > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li > a:focus {
    color: #777;
}
.with-nav-tabs.panel-default .nav-tabs > .open > a,
.with-nav-tabs.panel-default .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-default .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-default .nav-tabs > li > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li > a:focus {
    color: #777;
	background-color: #ddd;
	border-color: transparent;
}
.with-nav-tabs.panel-default .nav-tabs > li.active > a,
.with-nav-tabs.panel-default .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li.active > a:focus {
	color: #555;
	background-color: #fff;
	border-color: #ddd;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #f5f5f5;
    border-color: #ddd;
}
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #777;
}
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #ddd;
}
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #555;
}
/********************************************************************/
/*** PANEL PRIMARY ***/
.with-nav-tabs.panel-primary .nav-tabs > li > a,
.with-nav-tabs.panel-primary .nav-tabs > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
    color: #fff;
}
.with-nav-tabs.panel-primary .nav-tabs > .open > a,
.with-nav-tabs.panel-primary .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-primary .nav-tabs > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
	color: #fff;
	background-color: #3071a9;
	border-color: transparent;
}
.with-nav-tabs.panel-primary .nav-tabs > li.active > a,
.with-nav-tabs.panel-primary .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.active > a:focus {
	color: #428bca;
	background-color: #fff;
	border-color: #428bca;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #428bca;
    border-color: #3071a9;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #fff;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #3071a9;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    background-color: #4a9fe9;
}
/********************************************************************/
/*** PANEL SUCCESS ***/
.with-nav-tabs.panel-success .nav-tabs > li > a,
.with-nav-tabs.panel-success .nav-tabs > li > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li > a:focus {
	color: #3c763d;
}
.with-nav-tabs.panel-success .nav-tabs > .open > a,
.with-nav-tabs.panel-success .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-success .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-success .nav-tabs > li > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li > a:focus {
	color: #3c763d;
	background-color: #d6e9c6;
	border-color: transparent;
}
.with-nav-tabs.panel-success .nav-tabs > li.active > a,
.with-nav-tabs.panel-success .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li.active > a:focus {
	color: #3c763d;
	background-color: #fff;
	border-color: #d6e9c6;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #dff0d8;
    border-color: #d6e9c6;
}
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #3c763d;
}
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #d6e9c6;
}
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #3c763d;
}
/********************************************************************/
/*** PANEL INFO ***/
.with-nav-tabs.panel-info .nav-tabs > li > a,
.with-nav-tabs.panel-info .nav-tabs > li > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li > a:focus {
	color: #31708f;
}
.with-nav-tabs.panel-info .nav-tabs > .open > a,
.with-nav-tabs.panel-info .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-info .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-info .nav-tabs > li > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li > a:focus {
	color: #31708f;
	background-color: #bce8f1;
	border-color: transparent;
}
.with-nav-tabs.panel-info .nav-tabs > li.active > a,
.with-nav-tabs.panel-info .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li.active > a:focus {
	color: #31708f;
	background-color: #fff;
	border-color: #bce8f1;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #d9edf7;
    border-color: #bce8f1;
}
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #31708f;
}
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #bce8f1;
}
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #31708f;
}
/********************************************************************/
/*** PANEL WARNING ***/
.with-nav-tabs.panel-warning .nav-tabs > li > a,
.with-nav-tabs.panel-warning .nav-tabs > li > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li > a:focus {
	color: #8a6d3b;
}
.with-nav-tabs.panel-warning .nav-tabs > .open > a,
.with-nav-tabs.panel-warning .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-warning .nav-tabs > li > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li > a:focus {
	color: #8a6d3b;
	background-color: #faebcc;
	border-color: transparent;
}
.with-nav-tabs.panel-warning .nav-tabs > li.active > a,
.with-nav-tabs.panel-warning .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li.active > a:focus {
	color: #8a6d3b;
	background-color: #fff;
	border-color: #faebcc;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #fcf8e3;
    border-color: #faebcc;
}
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #8a6d3b;
}
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #faebcc;
}
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #8a6d3b;
}
/********************************************************************/
/*** PANEL DANGER ***/
.with-nav-tabs.panel-danger .nav-tabs > li > a,
.with-nav-tabs.panel-danger .nav-tabs > li > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li > a:focus {
	color: #a94442;
}
.with-nav-tabs.panel-danger .nav-tabs > .open > a,
.with-nav-tabs.panel-danger .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-danger .nav-tabs > li > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li > a:focus {
	color: #a94442;
	background-color: #ebccd1;
	border-color: transparent;
}
.with-nav-tabs.panel-danger .nav-tabs > li.active > a,
.with-nav-tabs.panel-danger .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li.active > a:focus {
	color: #a94442;
	background-color: #fff;
	border-color: #ebccd1;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #f2dede; /* bg color */
    border-color: #ebccd1; /* border color */
}
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #a94442; /* normal text color */
}
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #ebccd1; /* hover bg color */
}
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff; /* active text color */
    background-color: #a94442; /* active bg color */
}
</style>

  <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
  <div class="col-md-12 col-xs-12">
  	<div class="x_panel">

  		<div class="x_title">
  			<h2>Informasi Bangunan</small></h2>

  			<ul class="nav navbar-right panel_toolbox">
  				<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
  				</li>
  				<li class="dropdown">
  					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
  							class="fa fa-wrench"></i></a>
  					<ul class="dropdown-menu" role="menu">
  						<li><a href="#">Settings 1</a>
  						</li>
  						<li><a href="#">Settings 2</a>
  						</li>
  					</ul>
  				</li>
  				<li><a class="close-link"><i class="fa fa-close"></i></a>
  				</li>
  			</ul>
  			<div class="clearfix"></div>
  		</div>

  		<div class="form-horizontal form-label-left">
  			<br />
  			<!-- <form class="form-horizontal form-label-left"> -->
  			<!-- <input type="text" class="form-control" name="ID_PENGELOLA" placeholder="Nomor KTP" value="<?php echo $kodeunik; ?>" > -->
  		<div style="display:block" id="form_detail">
		
      <div class = "col-md-6 col-sm-12 col-xs-12">
  			<div class="form-group">
			  <input type="hidden" name="id_pengajuan" value="<?php echo $id_pengajuan;?>">
  				<label class="control-label ">Nama Bangunan</label>
					<input type="text" class="form-control" name="nama_bangunan" placeholder="Nama Bangunan" readonly="" value="<?php echo $Bangunan[0]['NM_BANGUNAN'];?>">

  				<label class="control-label ">Lokasi Bangunan</label>
					<input type="text" class="form-control" name="fungsi_bangunan" placeholder="Lokasi Bangunan" readonly="" value="<?php echo $Bangunan[0]['LOKASI_BANGUNAN'];?>">

  				<label class="control-label ">Provinsi</label>
  				<input type="text" class="form-control" name="provinsi_bangunan" placeholder="Provinsi" readonly="" value="<?php echo $Bangunan[0]['PROVINSI'];?>">

  				<label class="control-label ">Kota</label>
  				<input type="text" class="form-control" name="kota_bangunan" placeholder="Kota" readonly="" value="<?php echo $Bangunan[0]['KOTA'];?>">

  				<label class="control-label ">Kecamatan</label>
  				<input type="text" class="form-control" name="kecamatan_bangunan" placeholder="Kecamatan" readonly="" value="<?php echo $Bangunan[0]['KECAMATAN'];?>">

  				<label class="control-label ">Keluarahan</label>
  				<input type="text" class="form-control" name="keluarahn_bangunan" placeholder="Kelurahan" readonly=""value="<?php echo $Bangunan[0]['KELURAHAN'];?>">

          <label class="control-label ">Sublock</label>
  				<input type="text" class="form-control" name="sublock" placeholder="Sublock" readonly="" value="<?php echo $Bangunan[0]['SUBLOCK'];?>">

          <label class="control-label ">Zonasi</label>
  				<input type="text" class="form-control" name="zonasi" placeholder="Zonasi" readonly="" value="<?php echo $Bangunan[0]['ZONASI'];?>">

          <label class="control-label ">Fungsi Bangunan</label>
  				<div class="">
  					<input type="text" class="form-control" name="fungsi_bangunan" placeholder="Fungsi Bangunan" readonly="" value="<?php echo $Bangunan[0]['FUNGSI_BANGUNAN'];?>">
  				</div>
  				<label class="control-label ">Pengguna Bangunan</label>
  				<div class="">
  					<input type="text" class="form-control" name="penguna_bangunan" placeholder="Pengguna Bangunan" readonly="" value="<?php echo $Bangunan[0]['PENGGUNA_BANGUNAN'];?>">
  				</div>
  			</div>
      </div>

      <div class = "col-md-6 col-sm-12 col-xs-12">
  			<div class="form-group">
          <label class="control-label ">Nama Tower</label>
  				<div class="">
  					<input type="text" class="form-control" name="nama_tower" placeholder="Nama Tower" readonly="" value="<?php echo $Tower[0]['NAMA_TOWER'];?>">
  				</div>
  				<label class="control-label ">Tinggi Bangunan</label>
  				<div class="">
  					<input type="text" class="form-control" name="tinggi_bangunan" placeholder="Tinggi Bangunan" readonly="" value="<?php echo $Tower[0]['TINGGI_BANGUNAN'];?>">
  				</div>
  				<label class="control-label ">Jumlah Meizanin</label>
  				<div class="">
  					<input type="text" class="form-control" name="jml_meizanin" placeholder="Jumlah Meizanin" readonly="" value="<?php echo $Tower[0]['JML_MEIZANIN'];?>">
  				</div>
  				<label class="control-label ">Jumlah Basement</label>
  				<div class="">
  					<input type="text" class="form-control" name="jml_basement" placeholder="Jumlah Basement" readonly="" value="<?php echo $Tower[0]['JML_BASEMENT'];?>">
  				</div>
          <label class="control-label ">Luas Lantai Dasar</label>
  				<div class="">
  					<input type="text" class="form-control" name="luas_lantaidsr" placeholder="Luas Lantai Dasar" readonly="" value="<?php echo $Tower[0]['L_LANTAI_DASAR'];?>">
  				</div>
  				<label class="control-label ">Luas Bangunan</label>
  				<div class="">
  					<input type="text" class="form-control" name="luas_bangunan" placeholder="Luas Bangunan" readonly="" value="<?php echo $Tower[0]['L_BANGUNAN'];?>">
  				</div>
          <label class="control-label ">Luas Tapak Basement</label>
  				<div class="">
  					<input type="text" class="form-control" name="luas_tapakbsmnt" placeholder="Luas Tapak Basement" readonly="" value="<?php echo $Tower[0]['L_TAPAK_BASEMENT'];?>">
  				</div>
  				<label class="control-label ">Tingkat Hunian</label>
  				<div class="">
  					<input type="text" class="form-control" name="tingkat_hunian" placeholder="Tingkat Hunian" readonly="" value="<?php echo $Tower[0]['TINGKAT_HUNIAN'];?>">
  				</div>
    	</div>
  		</div>
		
		  <!-- <?php foreach($Tower as $x):?>
		<div class = "col-md-6 col-sm-12 col-xs-12">
  			<div class="form-group">
          <label class="control-label ">Nama Tower</label>
  				<div class="">
  					<input type="text" class="form-control" name="nama_tower" placeholder="Nama Tower" readonly="" value="<?php echo $x['NAMA_TOWER'];?>">
  				</div>
  				<label class="control-label ">Tinggi Bangunan</label>
  				<div class="">
  					<input type="text" class="form-control" name="tinggi_bangunan" placeholder="Tinggi Bangunan" readonly="" value="<?php echo $x['TINGGI_BANGUNAN'];?>">
  				</div>
  				<label class="control-label ">Jumlah Meizanin</label>
  				<div class="">
  					<input type="text" class="form-control" name="jml_meizanin" placeholder="Jumlah Meizanin" readonly="" value="<?php echo $x['JML_MEIZANIN'];?>">
  				</div>
  				<label class="control-label ">Jumlah Basement</label>
  				<div class="">
  					<input type="text" class="form-control" name="jml_basement" placeholder="Jumlah Basement" readonly="" value="<?php echo $x['JML_BASEMENT'];?>">
  				</div>
          <label class="control-label ">Luas Lantai Dasar</label>
  				<div class="">
  					<input type="text" class="form-control" name="luas_lantaidsr" placeholder="Luas Lantai Dasar" readonly="" value="<?php echo $x['L_LANTAI_DASAR'];?>">
  				</div>
  				<label class="control-label ">Luas Bangunan</label>
  				<div class="">
  					<input type="text" class="form-control" name="luas_bangunan" placeholder="Luas Bangunan" readonly="" value="<?php echo $x['L_BANGUNAN'];?>">
  				</div>
          <label class="control-label ">Luas Tapak Basement</label>
  				<div class="">
  					<input type="text" class="form-control" name="luas_tapakbsmnt" placeholder="Luas Tapak Basement" readonly="" value="<?php echo $x['L_TAPAK_BASEMENT'];?>">
  				</div>
  				<label class="control-label ">Tingkat Hunian</label>
  				<div class="">
  					<input type="text" class="form-control" name="tingkat_hunian" placeholder="Tingkat Hunian" readonly="" value="<?php echo $x['TINGKAT_HUNIAN'];?>">
  				</div>
        </div>
		<?php endforeach;?> -->

  			<div class="form-group">


  			</div>

  			</div>

        <div style="display:none;" class="form-horizontal form-label-left" id="detail">

          <div class="container">
            <div class="row">
              <div class="col-md-12">
                    <div class="panel with-nav-tabs panel-default">
                        <div class="panel-heading">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#tab1default" data-toggle="tab">Data Bangunan</a></li>
                                    <li><a href="#tab2default" data-toggle="tab">Data Tanah</a></li>
                                    <li><a href="#tab3default" data-toggle="tab">Data SIPPT</a></li>
                                    <li><a href="#tab4default" data-toggle="tab">IPTB</a></li>
                                    <li><a href="#tab5default" data-toggle="tab">Data Sumur Resapan</a></li>
                                    <li><a href="#tab6default" data-toggle="tab">Data Perizinan Lain Terkait</a></li>
									<li><a href="#tab7default" data-toggle="tab">Data Tower</a></li>
									
                                    <!-- <li><a href="#tab7default" data-toggle="tab">Data Teknis Terkait</a></li> -->
                                    <li><a href="#tab8default" data-toggle="tab">Data Perizinan IMB Yang Pernah Terbit</a></li>
                                    <li><a href="#tab9default" data-toggle="tab">Data Perizinan SLF Yang Pernah Terbit</a></li>
									<li><a href="#tab10default" data-toggle="tab">Data Gambar</a></li>
                                </ul>
                        </div>
                        <div class="panel-body">
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="tab1default">

                                  <div id="data_bangunan">
                                    <div class = "col-md-6 col-sm-12 col-xs-12">
									<label class="control-label ">Nama Bangunan</label>
					<input type="text" class="form-control" name="nama_bangunan" placeholder="Nama Bangunan" readonly="" value="<?php echo $Bangunan[0]['NM_BANGUNAN'];?>">

  				<label class="control-label ">Lokasi Bangunan</label>
					<input type="text" class="form-control" name="fungsi_bangunan" placeholder="Lokasi Bangunan" readonly="" value="<?php echo $Bangunan[0]['LOKASI_BANGUNAN'];?>">

  				<label class="control-label ">Provinsi</label>
  				<input type="text" class="form-control" name="provinsi_bangunan" placeholder="Provinsi" readonly="" value="<?php echo $Bangunan[0]['PROVINSI'];?>">

  				<label class="control-label ">Kota</label>
  				<input type="text" class="form-control" name="kota_bangunan" placeholder="Kota" readonly="" value="<?php echo $Bangunan[0]['KOTA'];?>">

  				<label class="control-label ">Kecamatan</label>
  				<input type="text" class="form-control" name="kecamatan_bangunan" placeholder="Kecamatan" readonly="" value="<?php echo $Bangunan[0]['KECAMATAN'];?>">

  				<label class="control-label ">Keluarahan</label>
  				<input type="text" class="form-control" name="keluarahn_bangunan" placeholder="Kelurahan" readonly=""value="<?php echo $Bangunan[0]['KELURAHAN'];?>">

          <label class="control-label ">Sublock</label>
  				<input type="text" class="form-control" name="sublock" placeholder="Sublock" readonly="" value="<?php echo $Bangunan[0]['SUBLOCK'];?>">

          <label class="control-label ">Zonasi</label>
  				<input type="text" class="form-control" name="zonasi" placeholder="Zonasi" readonly="" value="<?php echo $Bangunan[0]['ZONASI'];?>">

          <label class="control-label ">Fungsi Bangunan</label>
  				<div class="">
  					<input type="text" class="form-control" name="fungsi_bangunan" placeholder="Fungsi Bangunan" readonly="" value="<?php echo $Bangunan[0]['FUNGSI_BANGUNAN'];?>">
  				</div>
  				<label class="control-label ">Pengguna Bangunan</label>
  				<div class="">
  					<input type="text" class="form-control" name="penguna_bangunan" placeholder="Pengguna Bangunan" readonly="" value="<?php echo $Bangunan[0]['PENGGUNA_BANGUNAN'];?>">
  				</div>
                                    </div>

                        				</div>

                                </div>
                                <div class="tab-pane fade" id="tab2default">

                                  <div id="data_tanah">
                                    <div class = "col-md-6 col-sm-12 col-xs-12">
                        						  <div class="form-group">
                        								<label class="control-label ">Jumlah Sertifikat</label>
                        								<div class="">
                        									<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Jumlah Sertifikat" readonly="" value="<?php echo $Tanah[0]['ID_SERT_BANGUNAN'];?>">
                        								</div>
                        							</div>
                        							<div class="form-group">
                        								<label class="control-label ">Sertifikat 1</label>
                        								

                        								<label class="control-label ">Nomor Sertifikat</label>
                        								<div class="">
                        									<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Nomor Sertifikat" readonly="" value="<?php echo $Tanah[0]['NO_SERTIFIKAT'];?>">
                        								</div>

                        								<label class="control-label ">Luas Tanah</label>
                        								<div class="">
                        									<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Luas Tanah" readonly="" value="<?php echo $Tanah[0]['LUAS_TANAH'];?>">
                        								</div>
                        							</div>
                        							<div class="form-group">
                        								<label class="control-label ">Sertifikat 2</label>
                        								
                        								<label class="control-label ">Nomor Sertifikat</label>
                        								<div class="">
                        									<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Nomor Sertifikat" readonly="">
                        								</div>
                        								<label class="control-label ">Luas Tanah</label>
                        								<div class="">
                        									<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Luas Tanah" readonly="">
                        								</div>
                        							</div>
                        							<div class="form-group">
                        								<label class="control-label ">Sertifikat 3</label>
                        								

                        								<label class="control-label ">Nomor Sertifikat</label>
                        								<div class="">
                        									<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Nomor Sertifikat" readonly="">
                        								</div>
                        								<label class="control-label ">Luas Tanah</label>
                        								<div class="">
                        									<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Luas Tanah" readonly="">
                        								</div>
                        							</div>
                        							<div class="form-group">
                        								<label class="control-label "> Total Luas Tanah</label>
                        								<div class="">
                        									<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Total Luas Tanah" readonly="">
                        								</div>
                        							</div>

                        							<div class="form-group">
                        								<label class="control-label ">Jumlah Objek PBB</label>
                        								<div class="">
                        									<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Jumbal Objek PBB" readonly="" value="<?php echo $Tanah[0]['ID_PAJAK_BANGUNAN'];?>">
                        								</div>
                        							</div>
                        							<div class="form-group">
                        								<label class="control-label ">Objek Pajak PBB 1</label>
                        								
                        								<label class="control-label ">Nomor Objek Pajak PBB 1</label>
                        								<div class="">
                        									<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Nomor Objek Pajak PBB" readonly="" value="<?php echo $Tanah[0]['NOP'];?>">
                        								</div>
                        							</div>

                                    </div>
                                    <div class = "col-md-6 col-sm-12 col-xs-12">
                                      <div class="form-group">
                        								<label class="control-label ">Luas Bumi</label>
                        								<div class="">
                        									<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Luas Bumi" readonly="" value="<?php echo $Tanah[0]['LUAS_BUMI'];?>">
                        								</div>
                        								<label class="control-label ">Luas Bangunan</label>
                        								<div class="">
                        									<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Luas Bangunan" readonly="" value="<?php echo $Tanah[0]['LUAS_BANGUNAN'];?>">
                        								</div>
                        							</div>
                        							<div class="form-group">
                        								<label class="control-label ">Objek Pajak PBB 2</label>
                        								
                        								<label class="control-label ">Nomor Objek Pajak PBB 2</label>
                        								<div class="">
                        									<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Nomor Objek Pajak PBB" readonly="">
                        								</div>
                        							</div>
                        							<div class="form-group">
                        								<label class="control-label ">Luas Bumi</label>
                        								<div class="">
                        									<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Luas Bumi" readonly="">
                        								</div>
                        								<label class="control-label ">Luas Bangunan</label>
                        								<div class="">
                        									<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Luas Bangunan" readonly="">
                        								</div>
                        							</div>
                        							<div class="form-group">
                        								<label class="control-label ">Objek Pajak PBB 3</label>
                        								
                        								<label class="control-label ">Nomor Objek Pajak PBB 3</label>
                        								<div class="">
                        									<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Nomor Objek Pajak PBB" readonly="">
                        								</div>
                        							</div>
                        							<div class="form-group">
                        								<label class="control-label ">Luas Bumi</label>
                        								<div class="">
                        									<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Luas Bumi" readonly="">
                        								</div>
                        								<label class="control-label ">Luas Bangunan</label>
                        								<div class="">
                        									<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Luas Bangunan" readonly="">
                        								</div>
                        							</div>
                        							<div class="form-group">
                        								<label class="control-label ">Luas Total Bumi Bersama Dalam PBB</label>
                        								<div class="">
                        									<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Luas Tanah" readonly="">
                        								</div>
                        								<label class="control-label ">Luas Total Bangunan Bersama Dalam PBB</label>
                        								<div class="">
                        									<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Luas Tanah" readonly="">
                        								</div>
                        							</div>
                        							
                                    </div>
                        					</div>
                                </div>
                                <div class="tab-pane fade" id="tab3default">
                                  <div  class="form-horizontal form-label-left" id="data_sippt">
                                    <div class = "col-md-6 col-sm-12 col-xs-12">
                      								  <div class="form-group">
                      									<label class="control-label ">Nomor SIPPT/IPPTI/IPPTR</label>
                      									<div class="">
                      										<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Nomor SIPPT/IPPTI/IPPTR" readonly="" value="<?php echo $SIPPT[0]['NO_SIPPT'];?>">
                      									</div>
                      									</div>
														  <div class="form-group">
														<label class="control-label ">Tanggal SIPPT</label>
                      									<div class="">
                      										<input type="text" class="form-control" name="tgl_sippt" placeholder="Tanggal SIPPT" readonly="" value="<?php echo $SIPPT[0]['TGL_SIPPT'];?>">
                      									</div>
                      									</div>
                      									<div class="form-group">
                      									<label class="control-label ">Luas Lahan</label>
                      									<div class="">
                      										<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Luas Lahan" readonly=""value="<?php echo $SIPPT[0]['LUAS_LAHAN'];?>">
                      									</div>
                      									</div>
                      									<div class="form-group">
                      									<label class="control-label ">Luas Lahan Yang Belum Terbangun</label>
                      									<div class="">
                      										<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Luas Lahan Yang Belum Terbangun" readonly="">
                      									</div>
                      									</div>

                      									<div class="form-group">
                      									<label class="control-label ">Jumlah Bangunan Terbangun Dalam Area</label>
                      									<div class="">
                      										<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Jumlah Bangunan Terbangun" readonly="">
                      									</div>
                      									</div>

                      									<div class="form-group">
                      									<label class="control-label "> Sanksi</label>
                      									<div class="">
                      										<select class="form-control" name="PROVINSI" id="prov" >
                      										<option value="">Ada</option>
                      										<option value="dirut">Tidak Ada</option>
                      										</select>
                      									</div>
                      									</div>

                      									<div class="form-group">
                      									<label class="control-label ">Sanksi 1</label>
                      									<div class="">
                      										<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Sanksi" readonly="">
                      									</div>
                      									</div>
                      									<div class="form-group">
                      									<label class="control-label ">Sanksi 2</label>
                      									<div class="">
                      										<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Sanksi" readonly="">
                      									</div>
                      									</div>
                      									<div class="form-group">
                      									<label class="control-label ">Sanksi 3</label>
                      									<div class="">
                      										<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Sanksi" readonly="">
                      									</div>
                      									</div>
                                      </div>
                                      <div class = "col-md-6 col-sm-12 col-xs-12">
                      									<div class="form-group">
                      									<label class="control-label ">Total Luas Kewajiban Berupa Lahan</label>
                      									<div class="">
                      										<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Total Luas Kewajiban" readonly="">
                      									</div>
                      									
                      									
                      									<label class="control-label ">Total Luas Kewajiban Yang Belum Diserahkan</label>
                      									<div class="">
                      										<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Total Luas Kewajiban yang belum diserahkan" readonly="">
                      									</div>
                      									</div>

                      									<div class="form-group">
                      									<label class="control-label ">Pemenuhan Kewajiban SIPPT, S3PL dan Sanksi Lain</label>
                      									<div class="">
                      										<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Pemenuhan Kewajiban" readonly="">
                      									</div>
                      									</div>

                      									<div class="form-group">
                      									<label class="control-label ">Bulan</label>
                      									<div class="">
                      										<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Bulan" readonly="">
                      									</div>
                      									<label class="control-label ">Tahun</label>
                      									<div class="">
                      										<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Bulan" readonly="">
                      									</div>
                      									</div>
                                      </div>
                      						</div>
                                </div>
                                <div class="tab-pane fade" id="tab4default">
                                  <div class="form-horizontal form-label-left" id="iptb_arsitek">
                                    <div class = "col-md-6 col-sm-12 col-xs-12">
                        							<div class="form-group">
                        								

														<label class="control-label ">Nama IPTB</label>
                        								<div class="">
                        									<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Nama Perusahaan" readonly="" value="<?php echo $IPTB[0]['NM_IPTB'];?>">
                        								</div>


                        								<label class="control-label ">Nama Perusahaan</label>
                        								<div class="">
                        									<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Nama Perusahaan" readonly="" value="<?php echo $IPTB[0]['NM_PERUSAHAAN'];?>">
                        								</div>
                        								</div>

                        								<div class="form-group">
                        								<label class="control-label ">Nomor IPTB Arsitektur</label>
                        								<div class="">
                        									<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Nomor IPTB" readonly="" value="<?php echo $IPTB[0]['NO_IPTB'];?>">
                                        </div>

                        								<label class="control-label ">Jenis IPTB</label>
                        								<div class="">
                        									<select disabled class="form-control" name="Jenis_IPTB">

                        									<option value="<?php echo $IPTB[0]['JNS_IPTB'];?>"><?php echo $IPTB[0]['JNS_IPTB'];?></option>

                        									</select>
                        								</div>
                        								</div>

                        								<div class="form-group">
                        								<label class="control-label ">Nomor KTP</label>
                        								<div class="">
                        									<input type="number" class="form-control" name="NO_KTP" placeholder="Nomor KTP" readonly="" value="<?php echo $IPTB[0]['NO_KTP'];?>">
                        								</div>

                        								<label class="control-label ">Nomor NPWP</label>
                        								<div class="">
                        								<input type="number" class="form-control" name="NO_NPWP" placeholder="Nomor NPWP" readonly="" value="<?php echo $IPTB[0]['NO_NPWP'];?>">
                        								</div>
                        								</div>

                                       					 <div class="form-group">
                        								<label class="control-label ">Alamat Sesuai KTP</label>
                        									<input type="text" class="form-control" id="alamat_ktp" name="ALAMAT_KTP" placeholder="Alamat Sesuai KTP" readonly="" value="<?php echo $IPTB[0]['ALAMAT_KTP'];?>">

                        								</div>
                        								
                        								

                                       

                                       

                        								

                                        

                                        

                        								





                                      </div>
                                      <div class = "col-md-6 col-sm-12 col-xs-12">



                        								<div class="form-group">
                        								<label class="control-label ">Alamat Tempat Tinggal Sekarang</label>
                        									<input type="text" class="form-control" id="alamat_now" name="ALAMAT_SEKARANG" placeholder="Alamat Sesuai KTP" readonly="" >

                        								</div>

                        								

                        								
                                        

                            

                        								<div class="form-group">
                        								<label class="control-label ">Nomor Telepon</label>
                        								<div class="">
                        									<input type="number" class="form-control" name="NO_TELP" placeholder="Nomor Telepon" readonly="" value="<?php echo $IPTB[0]['NO_TELP'];?>">
                        								</div>

                        								<label class="control-label ">Alamat email</label>
                        								<div class="">
                        									<input type="email" class="form-control" name="EMAIL" placeholder="Alamat email" readonly="" value="<?php echo $IPTB[0]['EMAIL'];?>">
                        								</div>
                        								</div>
                        								<div class="form-group">


                        							
                                      </div>
                        							</div>
                        					</div>
                                </div>
                                <div class="tab-pane fade" id="tab5default">
                                  <div id="data_sumur">
                                        
                                          <div class="form-group">
                                          <label class="control-label ">Foto Tampak Bangunan Minimal 2 Sisi</label>
                                          <div class="">
                                            <select disabled class="form-control" name="Jenis_IPTB">
                                            <option value="">Ada</option>
                                            <option value="dirut">Tidak Ada</option>
                                            </select>
                                          </div>
                                          <div class="">
                                          <a href="#" download>Foto Tampak Bangunan Minimal 2 Sisi.jpg</a>
                                          </div>
                                          </div>

                                          

                                          <div class="form-group">
                                          <label class="control-label ">Jumlah Sumur Yang Berfungsi</label>
                                          <div class="">
                                            <input type="number" class="form-control" name="NAMA_IPTB" placeholder="Jumlah SUmur Yang Berfungsi" readonly="">
                                          </div>

                                          <label class="control-label ">Volume Sumur Yang Berfungsi</label>
                                          <div class="">
                                            <input type="number" class="form-control" name="NAMA_IPTB" placeholder="Volume SUmur Yang Berfungsi"readonly="">
                                          </div>

                                          <label class="control-label ">Jumlah Kolam resapan Yang Berfungsi</label>
                                          <div class="">
                                            <input type="number" class="form-control" name="NAMA_IPTB" placeholder="Jumlah Kolam resapan Yang Berfungsi" readonly="" value="<?php echo $SumurResapan[0]['JML_SUMUR_RESAPAN_BERFUNGSI']?>">
                                          </div>

                                          <label class="control-label ">Volume Kolam resapan Yang Berfungsi</label>
                                          <div class="">
                                            <input type="number" class="form-control" name="NAMA_IPTB" placeholder="Volume Kolam resapan Yang Berfungsi" readonly="" value="<?php echo $SumurResapan[0]['VLM_SUMUR_RESAPAN_BERFUNGSI']?>">
                                          </div>
                                          </div>

                                          <div class="form-group">
                                          <label class="control-label ">Kesediaan Sanksi Untuk Ketidaksesuaaian</label>
                                          <div class="">
                                            <input type="text" class="form-control" name="NAMA_IPTB" placeholder="Kesediaan Sanksi Untuk Ketidaksesuaaian" readonly="">
                                          </div>

                                          </div>

                                  </div>
                                </div>
                                <div class="tab-pane fade" id="tab6default">
                                  <div id="data_perizinanlain">
                        						<div class="form-group">
                        							<label class="control-label ">Sertifikat Laik Operasi</label>
                        							<!-- <div class="">
                        								<a href="#" download> Sertifikat Laik Operasi.pdf</a>
                        							</div> -->

                        							</div>
                        							<div class="form-group">
                        							<label class="control-label ">Nomor</label>
                        							<div class="">
                        								<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                        							</div>
                        							<label class="control-label ">Tanggal</label>
                        							<div class="">
                        								<input type="date" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                        							</div>
                        							</div>
                        							<div class="form-group">
                        							<label class="control-label ">Pengesahan Pemakaian Instalasi Proteksi Kebakaran</label>
                        							<!-- <div class="">
                        								<a href="#" download> Pengesahan Pemakaian Instalasi Proteksi Kebakaran.pdf</a>
                        							</div> -->

                        							</div>
                        							<div class="form-group">
                        							<label class="control-label ">Nomor</label>
                        							<div class="">
                        								<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                        							</div>
                        							<label class="control-label ">Tanggal</label>
                        							<div class="">
                        								<input type="date" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                        							</div>
                        							</div>
                        							<div class="form-group">
                        							<label class="control-label ">Pengesahan Pemakaian Pesawat Tenaga Produksi</label>
                        							<!-- <div class="">
                        							<a href="#" download> Pengesahan Pemakaian Pesawat Tenaga Produksi.pdf</a>
                        							</div> -->

                        							</div>
                        							<div class="form-group">
                        							<label class="control-label ">Nomor</label>
                        							<div class="">
                        								<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                        							</div>
                        							<label class="control-label ">Tanggal</label>
                        							<div class="">
                        								<input type="date" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                        							</div>
                        							</div>
                        							<div class="form-group">
                        							<label class="control-label ">Pengesahan Pemakaian Bejana Token</label>
                        							<!-- <div class="">
                        							<a href="#" download> Pengesahan Pemakaian Pesawat Tenaga Produksi.pdf</a>
                        							</div> -->

                        							</div>
                        							<div class="form-group">
                        							<label class="control-label ">Nomor</label>
                        							<div class="">
                        								<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                        							</div>
                        							<label class="control-label ">Tanggal</label>
                        							<div class="">
                        								<input type="date" class="form-control" name="NAMA_IPTB" placeholder="Nomor"readonly>
                        							</div>
                        							</div>
                        							<div class="form-group">
                        							<label class="control-label ">Pengesahan Pemakaian Instalasi Penyalur Petir</label>
                        							<!-- <div class="">
                        							<a href="#" download> Pengesahan Pemakaian Pesawat Tenaga Produksi.pdf</a>
                        							</div> -->

                        							</div>
                        							<div class="form-group">
                        							<label class="control-label ">Nomor</label>
                        							<div class="">
                        								<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                        							</div>
                        							<label class="control-label ">Tanggal</label>
                        							<div class="">
                        								<input type="date" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                        							</div>
                        							</div>
                        							<div class="form-group">
                        							<label class="control-label ">Pengesahan Pemakaian Pesawat Angkat dan Angkut Lift</label>
                        							<!-- <div class="">
                        							<a href="#" download> Pengesahan Pemakaian Pesawat Angkat dan Angkut Lift.pdf</a>
                        							</div> -->

                        							</div>
                        							<div class="form-group">
                        							<label class="control-label ">Nomor</label>
                        							<div class="">
                        								<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                        							</div>
                        							<label class="control-label ">Tanggal</label>
                        							<div class="">
                        								<input type="date" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                        							</div>
                        							</div>
                        							<div class="form-group">
                        							<label class="control-label ">Pengesahan Pemakaian Pesawat Angkat dan ANgkut Eskalator</label>
                        							<!-- <div class="">
                        							<a href="#" download> Pengesahan Pemakaian Pesawat Angkat dan ANgkut Eskalator.pdf</a>
                        							</div> -->

                        							</div>
                        							<div class="form-group">
                        							<label class="control-label ">Nomor</label>
                        							<div class="">
                        								<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                        							</div>
                        							<label class="control-label ">Tanggal</label>
                        							<div class="">
                        								<input type="date" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                        							</div>
                        							</div>
                        							<div class="form-group">
                        							<label class="control-label ">Pengesahan Pemakaian Pesawat Angkat dan Angkut Gondola</label>
                        							<!-- <div class="">
                        							<a href="#" download>Pengesahan Pemakaian Pesawat Angkat dan Angkut Gondola.pdf</a>
                        							</div> -->

                        							</div>
                        							<div class="form-group">
                        							<label class="control-label ">Nomor</label>
                        							<div class="">
                        								<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                        							</div>
                        							<label class="control-label ">Tanggal</label>
                        							<div class="">
                        								<input type="date" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                        							</div>
                        							</div>
                        							<div class="form-group">
                        							<label class="control-label ">Pengesahan Pemakaian Instalasi Listriki</label>
                        							<!-- <div class="">
                        							<a href="#" download> Pengesahan Pemakaian Instalasi Listriki.pdf</a>
                        							</div> -->

                        							</div>
                        							<div class="form-group">
                        							<label class="control-label ">Nomor</label>
                        							<div class="">
                        								<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                        							</div>
                        							<label class="control-label ">Tanggal</label>
                        							<div class="">
                        								<input type="date" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                        							</div>
                        							</div>
                        							<div class="form-group">
                        							<label class="control-label ">Pengesahan Pemakaian Pesawat UAP</label>
                        							<!-- <div class="">
                        							<a href="#" download> Pengesahan Pemakaian Pesawat UAP.pdf</a>
                        							</div> -->

                        							</div>
                        							<div class="form-group">
                        							<label class="control-label ">Nomor</label>
                        							<div class="">
                        								<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                        							</div>
                        							<label class="control-label ">Tanggal</label>
                        							<div class="">
                        								<input type="date" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                        							</div>
                        							</div>
                        					</div>
                                </div>
                                <div class="tab-pane fade" id="tab7default">
                                  <div id="data_tower">
                        						<div class="form-group">
													
													<label class="control-label ">Nama Tower</label>
													<div class="">
														<input type="text" class="form-control" name="nama_tower" placeholder="Nama Tower" readonly="" value="<?php echo $Tower[0]['NAMA_TOWER'];?>">
													</div>
													<label class="control-label ">Tinggi Bangunan</label>
													<div class="">
														<input type="text" class="form-control" name="tinggi_bangunan" placeholder="Tinggi Bangunan" readonly="" value="<?php echo $Tower[0]['TINGGI_BANGUNAN'];?>">
													</div>
													<label class="control-label ">Jumlah Meizanin</label>
													<div class="">
														<input type="text" class="form-control" name="jml_meizanin" placeholder="Jumlah Meizanin" readonly="" value="<?php echo $Tower[0]['JML_MEIZANIN'];?>">
													</div>
													<label class="control-label ">Jumlah Basement</label>
													<div class="">
														<input type="text" class="form-control" name="jml_basement" placeholder="Jumlah Basement" readonly="" value="<?php echo $Tower[0]['JML_BASEMENT'];?>">
													</div>
													<label class="control-label ">Luas Lantai Dasar</label>
													<div class="">
														<input type="text" class="form-control" name="luas_lantaidsr" placeholder="Luas Lantai Dasar" readonly="" value="<?php echo $Tower[0]['L_LANTAI_DASAR'];?>">
													</div>
													<label class="control-label ">Luas Bangunan</label>
													<div class="">
														<input type="text" class="form-control" name="luas_bangunan" placeholder="Luas Bangunan" readonly="" value="<?php echo $Tower[0]['L_BANGUNAN'];?>">
													</div>
													<label class="control-label ">Luas Tapak Basement</label>
													<div class="">
														<input type="text" class="form-control" name="luas_tapakbsmnt" placeholder="Luas Tapak Basement" readonly="" value="<?php echo $Tower[0]['L_TAPAK_BASEMENT'];?>">
													</div>
													<label class="control-label ">Tingkat Hunian</label>
													<div class="">
														<input type="text" class="form-control" name="tingkat_hunian" placeholder="Tingkat Hunian" readonly="" value="<?php echo $Tower[0]['TINGKAT_HUNIAN'];?>">
													</div>
                        						</div>
                        					</div>
                                </div>
                                <div class="tab-pane fade" id="tab8default">
                                  <div id="data_imblama">
                                    <div class="form-group">
                                      <label class="control-label ">Nomor IMB</label>
									  <div class="">
                                        <input type="text" class="form-control" name="NAMA_IPTB" placeholder="Nama IPTB" readonly value="<?php echo $IMB[0]['NO_IMB'];?>">
                                      </div>
                                      </div>

                                      <div class="form-group">
                                      <label class="control-label ">Tanggal</label>
                                      <div class="">
                                        <input type="text" class="form-control" name="NAMA_IPTB" placeholder="Nama IPTB" readonl value="<?php echo $IMB[0]['TGL_IMB'];?>">
                                      </div>
                                      </div>

                                      <div class="form-group">
                                      <label class="control-label ">Luas Seluruh Lantai</label>
                                      <div class="">
                                        <input type="number" class="form-control" name="NAMA_IPTB" placeholder="Luas Seluruh Lantai" readonly value="<?php echo $IMB[0]['L_BANGUNAN'];?>">
                                      </div>
                                      </div>

                                      <div class="form-group">
                                      <label class="control-label ">Jumlah Tower / Massa Bangunan</label>
                                      <div class="">
                                        <input type="number" class="form-control" name="NAMA_IPTB" placeholder="Jumlah Tower / Massa Bangunan" readonly>
                                      </div>
                                      </div>
                                      <div class="form-group">
                                      <label class="control-label ">Gambar Lampiran Bidang Arsitektur</label>
                                      <div class="">
                                        <select disabled class="form-control" name="Jenis_IPTB">
                                        <option value="">Ada</option>
                                        <option value="dirut">Tidak Ada</option>
                                        </select>
                                      </div>
                                      </div>
                                      <div class="form-group">
                                      <label class="control-label ">Keterangan Rencana Kota (KRK)</label>
                                      <div class="">
                                        <select  disabled class="form-control" name="Jenis_IPTB">
                                        <option value="">Ada</option>
                                        <option value="dirut">Tidak Ada</option>
                                        </select>
                                      </div>
                                      </div>
                                      <div class="form-group">
                                      <label class="control-label ">Nomor KRK</label>
                                      <div class="">
                                        <input type="text" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly value="<?php echo $SLF[0]['NO_KRK'];?>">
                                      </div>
                                      <label class="control-label ">Tanggal</label>
                                      <div class="">
                                        <input type="text" class="form-control" name="NAMA_IPTB" placeholder="Nama IPTB" readonly value="<?php echo $SLF[0]['TGL_KRK'];?>">
                                      </div>
                                      </div>

                                      <div class="form-group">
                                      <label class="control-label "> Rencana Tata Letak Bangunan (RTLB)</label>
                                      <div class="">
                                        <select disabled class="form-control" name="Jenis_IPTB">
                                        <option value="">Ada</option>
                                        <option value="dirut">Tidak Ada</option>
                                        </select>
                                      </div>
                                      </div>
                                      <div class="form-group">
                                      <label class="control-label ">Nomor RTLB</label>
                                      <div class="">
                                        <input type="text" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                                      </div>
                                      <label class="control-label ">Tanggal</label>
                                      <div class="">
                                        <input type="date" class="form-control" name="NAMA_IPTB" placeholder="Nama IPTB" readonly>
                                      </div>
                                      </div>
                                      <div class="form-group">
                                      <label class="control-label ">GPA</label>
                                      <div class="">
                                        <select disabled class="form-control" name="Jenis_IPTB">
                                        <option value="">Ada</option>
                                        <option value="dirut">Tidak Ada</option>
                                        </select>
                                      </div>
                                      </div>
                                      <div class="form-group">
                                      <label class="control-label ">Nomor GPA</label>
                                      <div class="">
                                        <input type="text" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                                      </div>
                                      <label class="control-label ">Tanggal</label>
                                      <div class="">
                                        <input type="date" class="form-control" name="NAMA_IPTB" placeholder="Nama IPTB" readonly>
                                      </div>
                                      </div>
                                  </div>
                                </div>
                                <div class="tab-pane fade" id="tab9default">
                                  <div  id="slf_pernahterbit">
                        						<div class="form-group">
                        							<label class="control-label ">Nomor SLF</label>
                        							<div class="">
                                        <input type="text" class="form-control" name="nomor_slf" placeholder="Nomor SLF" readonly>
                                      </div>
                        							</div>

                        							<div class="form-group">
                        							<label class="control-label ">Tanggal</label>
                        							<div class="">
                        								<input type="date" class="form-control" name="nomor_slf" placeholder="Nama IPTB" readonly>
                        							</div>
                        							</div>

                        							<div class="form-group">
                        							<label class="control-label ">Luas Seluruh Lantai</label>
                        							<div class="">
                        								<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Luas Seluruh Lantai" readonly>
                        							</div>
                        							</div>

                        							<div class="form-group">
                        							<label class="control-label ">Jumlah Tower / Massa Bangunan</label>
                        							<div class="">
                        								<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Jumlah Tower / Massa Bangunan" readonly>
                        							</div>
                        							</div>
                        							<div class="form-group">
                        							<label class="control-label ">Gambar Lampiran Bidang Arsitektur</label>
                        							<div class="">
                        								<select disabled class="form-control" name="Jenis_IPTB">
                        								<option value="">Ada</option>
                        								<option value="dirut">Tidak Ada</option>
                        								</select>
                        							</div>
                        							</div>
                        							<div class="form-group">
                        							<label class="control-label ">Keterangan Rencana Kota (KRK)</label>
                        							<div class="">
                        								<select disabled class="form-control" name="Jenis_IPTB">
                        								<option value="">Ada</option>
                        								<option value="dirut">Tidak Ada</option>
                        								</select>
                        							</div>
                        							</div>
                        							<div class="form-group">
                        							<label class="control-label ">Nomor</label>
                        							<div class="">
                        								<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly value="<?php echo $SLF[0]['NO_KRK'];?>">
                        							</div>
                        							<label class="control-label ">Tanggal</label>
                        							<div class="">
                        								<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Nama IPTB" readonly value="<?php echo $SLF[0]['TGL_KRK'];?>">
                        							</div>
                        							</div>

                        							<div class="form-group">
                        							<label class="control-label "> Rencana Tata Letak Bangunan (RTLB)</label>
                        							<div class="">
                        								<select disabled class="form-control" name="Jenis_IPTB">
                        								<option value="">Ada</option>
                        								<option value="dirut">Tidak Ada</option>
                        								</select>
                        							</div>
                        							</div>
                        							<div class="form-group">
                        							<label class="control-label ">Nomor</label>
                        							<div class="">
                        								<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                        							</div>
                        							<label class="control-label ">Tanggal</label>
                        							<div class="">
                        								<input type="date" class="form-control" name="NAMA_IPTB" placeholder="Nama IPTB" readonly>
                        							</div>
                        							</div>
                        							<div class="form-group">
                        							<label class="control-label ">GPA</label>
                        							<div class="">
                        								<select disabled class="form-control" name="Jenis_IPTB">
                        								<option value="">Ada</option>
                        								<option value="dirut">Tidak Ada</option>
                        								</select>
                        							</div>
                        							</div>
                        							<div class="form-group">
                        							<label class="control-label ">Nomor</label>
                        							<div class="">
                        								<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                        							</div>
                        							<label class="control-label ">Tanggal</label>
                        							<div class="">
                        								<input type="date" class="form-control" name="NAMA_IPTB" placeholder="Nama IPTB" readonly>
                        							</div>
                        							</div>
                        					</div>
                                </div>
								<div class="tab-pane fade" id="tab10default">
									<div id="data_gambar">
										<table id="m_artikel" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>Arsitek</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td><label class="control-label ">Site Plan.dwg</label></td>
													<td><a href="#" download><button type="submit" name="submit" class="btn btn-success" style="background-color:orange;">Unduh</button></a></td>
												</tr>
												<tr>
													<td><label class="control-label ">SRAH & Detail SRAH.dwg</label></td>
													<td><a href="#" download><button type="submit" name="submit" class="btn btn-success" style="background-color:orange;">Unduh</button></a></td>
												</tr>
												<tr>
													<td><label class="control-label ">Potongan.dwg</label></td>
													<td><a href="#" download><button type="submit" name="submit" class="btn btn-success" style="background-color:orange;">Unduh</button></a></td>
												</tr>
												<tr>
													<td><label class="control-label ">Tampak.dwg</label></td>
													<td><a href="#" download><button type="submit" name="submit" class="btn btn-success" style="background-color:orange;">Unduh</button></a></td>
												</tr>
											</tbody>
											<thead>
												<tr>
													<th>Struktur</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td><label class="control-label ">Denah Pondasi</label></td>
													<td><a href="#" download><button type="submit" name="submit" class="btn btn-success" style="background-color:orange;">Unduh</button></a></td>
												
												</tr>
												<tr>
													<td><label class="control-label ">Denah Balok, Kolom dan Plat Lantai.dwg</label></td>
													<td><a href="#" download><button type="submit" name="submit" class="btn btn-success" style="background-color:orange;">Unduh</button></a></td>
												</tr>
												<tr>
													<td><label class="control-label ">Detail Balok, Kolom dan Plat Lantai.dwg</label></td>
													<td><a href="#" download><button type="submit" name="submit" class="btn btn-success" style="background-color:orange;">Unduh</button></a></td>
												</tr>
												<tr>
													<td><label class="control-label ">Detail Shear wall dan core wall.dwg</label></td>
													<td><a href="#" download><button type="submit" name="submit" class="btn btn-success" style="background-color:orange;">Unduh</button></a></td>
												</tr>
											</tbody>
											<thead>
												<tr>
													<th>ME</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td><label class="control-label ">Diagram skematik sistem distribusi listrik.dwg</label></td>
													<td><a href="#" download><button type="submit" name="submit" class="btn btn-success" style="background-color:orange;">Unduh</button></a></td>
												</tr>
												<tr>
													<td><label class="control-label ">Instaliasi Penyalur petir.dwg</label></td>
													<td><a href="#" download><button type="submit" name="submit" class="btn btn-success" style="background-color:orange;">Unduh</button></a></td>
												</tr>
												<tr>
													<td><label class="control-label ">Diagram skematik sound sistem.dwg</label></td>
													<td><a href="#" download><button type="submit" name="submit" class="btn btn-success" style="background-color:orange;">Unduh</button></a></td>
												</tr>
												<tr>
													<td><label class="control-label ">Diagram skematik Fire Alarm.dwg</label></td>
													<td><a href="#" download><button type="submit" name="submit" class="btn btn-success" style="background-color:orange;">Unduh</button></a></td>
												</tr>
												<tr>
													<td><label class="control-label ">Diagram skematik Diagram Skematik Telepon.dwg</label></td>
													<td><a href="#" download><button type="submit" name="submit" class="btn btn-success" style="background-color:orange;">Unduh</button></a></td>
												</tr>
												<tr>
													<td><label class="control-label ">Diagram skematik Diagram skematik pipa pemadam kebakaran.dwg</label></td>
													<td><a href="#" download><button type="submit" name="submit" class="btn btn-success" style="background-color:orange;">Unduh</button></a></td>
												</tr>
												<tr>
													<td><label class="control-label ">Diagram skematik Diagram skematik air bersih.dwg</label></td>
													<td><a href="#" download><button type="submit" name="submit" class="btn btn-success" style="background-color:orange;">Unduh</button></a></td>
												</tr>
												<tr>
													<td><label class="control-label ">Diagram skematik Diagram skematik air kotor.dwg</label></td>
													<td><a href="#" download><button type="submit" name="submit" class="btn btn-success" style="background-color:orange;">Unduh</button></a></td>
												</tr>
												<tr>
													<td><label class="control-label ">Diagram skematik Diagram skematik air hujan.dwg</label></td>
													<td><a href="#" download><button type="submit" name="submit" class="btn btn-success" style="background-color:orange;">Unduh</button></a></td>
												</tr>
												<tr>
													<td><label class="control-label ">Diagram skematik Diagram skematik Lift.dwg</label></td>
													<td><a href="#" download><button type="submit" name="submit" class="btn btn-success" style="background-color:orange;">Unduh</button></a></td>
												</tr>
												<tr>
													<td><label class="control-label ">Diagram skematik Diagram skematik Gondola.dwg</label></td>
													<td><a href="#" download><button type="submit" name="submit" class="btn btn-success" style="background-color:orange;">Unduh</button></a></td>
												</tr>
												<tr>
													<td><label class="control-label ">Diagram skematik Diagram skematik eskalator.dwg</label></td>
													<td><a href="#" download><button type="submit" name="submit" class="btn btn-success" style="background-color:orange;">Unduh</button></a></td>
												</tr>
												<tr>
													<td><label class="control-label ">Diagram skematik Diagram skematik pressurized Fan.dwg</label></td>
													<td><a href="#" download><button type="submit" name="submit" class="btn btn-success" style="background-color:orange;">Unduh</button></a></td>
												</tr>
												<tr>
													<td><label class="control-label ">Diagram skematik Exhaust.dwg</label></td>
													<td><a href="#" download><button type="submit" name="submit" class="btn btn-success" style="background-color:orange;">Unduh</button></a></td>
												</tr>
												<tr>
													<td><label class="control-label ">Diagram skematik lintakehaust.dwg</label></td>
													<td><a href="#" download><button type="submit" name="submit" class="btn btn-success" style="background-color:orange;">Unduh</button></a></td>
												</tr>
												<tr>
													<td><label class="control-label ">Diagram skematik sistem pendingin.dwg</label></td>
													<td><a href="#" download><button type="submit" name="submit" class="btn btn-success" style="background-color:orange;">Unduh</button></a></td>
												</tr>

											</tbody>
										
										</table>
										
										
										</div>
									</div>
								</div>

                            </div>
                        </div>
                    </div>
                </div>
              </div>
          </div>




              </div>

              <div class="form-group">

                  <input type="checkbox" style="margin-left:230px; margin-top:30px" onchange="isChecked(this, 'btn1')"> Dengan ini saya menyatakan bahwa data yang akan kami ajukan sebagai permohonan benar adanya <i>(Jika Diperlukan Cek Detail)
           </i>
              </div>



        <!--<div class="ln_solid"></div> -->
  			<div class="form-group">
  				<div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">

  					<button type="submit" name="submit" class="btn btn-info" onclick="showDetail()">Cek Detail</button>

  					<a href="<?php echo base_url('formulir/add/'.$id_pengajuan);?>"><button type="submit" name="submit" class="btn btn-success"
  						style="background-color:orange;" id="btn1" disabled>Lanjutkan</button></a>

  				</div>
  			</div>
  		</div>

  	</div>



</div>
<br/>


<script type="text/javascript">
  function change() {
    var a = $('#alamat_ktp').val();
    var b = $("#prov").val();
    var c = $("#kecamatan").val();
    var d = $("#kelurahan").val();
    var e = $("#kota").val();

    $('#alamat_now').val(a);
    $("#prov2").val(b);
    $("#kecamatan2").val(c);
    $("#kelurahan2").val(d);
    $("#kota2").val(e);
  }

  function isChecked(checkbox, btn1) {
    var button = document.getElementById(btn1);

    if (checkbox.checked === true) {
        button.disabled = "";
    } else {
        button.disabled = "disabled";
    }
}

  function showDetail() {
      var x = document.getElementById("detail");
    var y = document.getElementById("form_detail")
      if 	(x.style.display === "none" ) {
        x.style.display = "block";
      y.style.display = "none";
      } 	else {
        x.style.display = "none";
      y.style.display = "block";
      }
  }
  function showDataBangunan() {
      var x = document.getElementById("data_bangunan");
      if 	(x.style.display === "none") {
        x.style.display = "block";
      } 	else {
        x.style.display = "none";
      }
  }
  function showDataTanah() {
      var x = document.getElementById("data_tanah");
      if 	(x.style.display === "none") {
        x.style.display = "block";
      } 	else {
        x.style.display = "none";
      }
  }
  function ShowDataSIPPT() {
      var x = document.getElementById("data_sippt");
      if 	(x.style.display === "none") {
        x.style.display = "block";
      } 	else {
        x.style.display = "none";
      }
  }
  function ShowIptbArsitek() {
      var x = document.getElementById("iptb_arsitek");
      if 	(x.style.display === "none") {
        x.style.display = "block";
      } 	else {
        x.style.display = "none";
      }
  }
  function ShowIptbLal() {
      var x = document.getElementById("iptb_lal");
      if 	(x.style.display === "none") {
        x.style.display = "block";
      } 	else {
        x.style.display = "none";
      }
  }
  function ShowIptbLak() {
      var x = document.getElementById("iptb_lak");
      if 	(x.style.display === "none") {
        x.style.display = "block";
      } 	else {
        x.style.display = "none";
      }
  }
  function ShowIptbSdp() {
      var x = document.getElementById("iptb_sdp");
      if 	(x.style.display === "none") {
        x.style.display = "block";
      } 	else {
        x.style.display = "none";
      }
  }
  function ShowIptbTug() {
      var x = document.getElementById("iptb_tug");
      if 	(x.style.display === "none") {
        x.style.display = "block";
      } 	else {
        x.style.display = "none";
      }
  }
  function ShowIptbTdg() {
      var x = document.getElementById("iptb_tdg");
      if 	(x.style.display === "none") {
        x.style.display = "block";
      } 	else {
        x.style.display = "none";
      }
  }
  function ShowDataSumur() {
      var x = document.getElementById("data_sumur");
      if 	(x.style.display === "none") {
        x.style.display = "block";
      } 	else {
        x.style.display = "none";
      }
  }
  function ShowDataIzinlain() {
      var x = document.getElementById("data_perizinanlain");
      if 	(x.style.display === "none") {
        x.style.display = "block";
      } 	else {
        x.style.display = "none";
      }
  }
  function ShowDataTeknis() {
      var x = document.getElementById("data_teknis");
      if 	(x.style.display === "none") {
        x.style.display = "block";
      } 	else {
        x.style.display = "none";
      }
  }
  function ShowDataIMBlama() {
      var x = document.getElementById("data_imblama");
      if 	(x.style.display === "none") {
        x.style.display = "block";
      } 	else {
        x.style.display = "none";
      }
  }
  function ShowSlfPernahTerbit() {
      var x = document.getElementById("slf_pernahterbit");
      if 	(x.style.display === "none") {
        x.style.display = "block";
      } 	else {
        x.style.display = "none";
      }
  }


</script>
