<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

</head>

<div class="container">
  <div class="card mb-3">
        <div class="card-header">
          <h3>Berkas Permohonan</h3>
        </div>
        <div class="card-body">

          <div class="table-responsive">
         
          </div>
        </div>
      </div>
</div>

<script>
function open_doc(a){
  $("#doc").show();
  $("#title").html(a);
}
</script>

<div id = "doc">
<table  id="m_artikel" class="table table-striped table-bordered">
    <h4><div id = title></div></h4>
    <thead>
      <tr>
        <th>Nama Surat</th>
        <th>Template Surat</th>
        <th>Unggah Surat</th>
        <th>Aksi</th>
      </tr>
    </thead>
    <tbody>

      <tr>

      <?php echo form_open_multipart('dokumen/upload_surat_ppk');?>
        <td><label class="">Surat Perjanjian Pemenuhan Kewajiban (PPK)</label></td>
        <td><a href="<?php echo base_url('assets/document/surat_ppk/template/1. Surat Pernyataan Pemohon untuk Kegiatan Permohonan SLF.docx');?>" Unduh class="btn btn-success" style="background-color:orange;">Unduh</a></td>
        <td><input type="file" name="file_surat_ppk"></td>
        <td><input class="btn btn-success" type="submit" value="upload" /></td>
      </form>
      </tr>

      <tr>
        <?php echo form_open_multipart('dokumen/upload_surat_kerjasama');?>
        <td><label class="">Surat Perjanjian Kerjasama antara Pemilik Tanah/Bangunan dan Pengelola Bangunan</label></td>
        <td><a href="#" Unduh><button type="submit" name="submit" class="btn btn-success" style="background-color:orange;">Unduh</button></a></td>
        <td><input type="file" name="file_surat_kerjasama"></td>
        <td><input class="btn btn-success" type="submit" value="upload" /></td>
        </form>
      </tr>

      <tr>
        <?php echo form_open_multipart('dokumen/upload_surat_iptbArsitek');?>
        <td><label class="">Surat Pernyataan IPTB Arsitek</label></td>
        <td><a href="<?php echo base_url('assets/document/surat_iptbArsitek/template/6. Surat Pernyataan IPTB Arsitek.docx');?>" Unduh class="btn btn-success" style="background-color:orange;">Unduh</a></td>
        <td><input type="file" name="file_surat_iptbArsitek"></td>
        <td><input class="btn btn-success" type="submit" value="upload" /></td>
        </form>
      </tr>
      <tr>
        <?php echo form_open_multipart('dokumen/upload_surat_iptbKonstruksi');?>
        <td><label class="">Surat Pernyataan IPTB Konstruksi</label></td>
        <td><a href="<?php echo base_url('assets/document/surat_iptbkonstruksi/template/7. Surat Pernyataan IPTB Konstruksi.docx');?>" Unduh class="btn btn-success" style="background-color:orange;">Unduh</a></td>
        <td><input type="file" name="file_surat_iptbKonstruksi"></td>
        <td><input class="btn btn-success" type="submit" value="upload" /></td>
        </form>
      </tr>
      <tr>
        <?php echo form_open_multipart('dokumen/upload_surat_iptbLAL');?>
        <td><label class="">Surat Pernyataan Pemilik IPTB Bidang Instalasi Listrik Arus Lemah (LAL)</label></td>
        <td><a href="<?php echo base_url('assets/document/surat_iptblal/template/8. Surat Pernyataan Pemilik IPTB Bidang Instalasi Listrik Arus Lemah.docx');?>" Unduh class="btn btn-success" style="background-color:orange;">Unduh</a></td>
        <td><input type="file" name="file_surat_iptbLAL"></td>
        <td><input class="btn btn-success" type="submit" value="upload" /></td>
        </form>
      </tr>
      <tr>
        <?php echo form_open_multipart('dokumen/upload_surat_iptbLAK');?>
        <td><label class="">Surat Pernyataan Pemilik IPTB Bidang Instalasi Listrik Arus Kuat (LAK)</label></td>
        <td><a href="<?php echo base_url('assets/document/surat_iptblak/template/9. Surat Pernyataan Pemilik IPTB Bidang Instalasi Listrik Arus Kuat.docx');?>" Unduh class="btn btn-success" style="background-color:orange;">Unduh</a></td>
        <td><input type="file" name="file_surat_iptbLAK"></td>
        <td><input class="btn btn-success" type="submit" value="upload" /></td>
        </form>
      </tr>
      <tr>
        <?php echo form_open_multipart('dokumen/upload_surat_iptbSDP');?>
        <td><label class="">Surat Pernyataan Pemilik IPTB BIdang Instalasi Sanitasi Drainase Pemipaan</label></td>
        <td><a href="<?php echo base_url('assets/document/surat_iptbSDP/template/10. Surat Pernyataan Pemilik IPTB Bidang Instalasi Sanitasi Drainase Pemipaan.docx');?>" Unduh class="btn btn-success" style="background-color:orange;">Unduh</a></td>
        <td><input type="file" name="file_surat_iptbSDP"></td>
        <td><input class="btn btn-success" type="submit" value="upload" /></td>
        </form>
      </tr>
      <tr>
        <?php echo form_open_multipart('dokumen/upload_surat_iptbTUG');?>
        <td><label class=""> Surat Pernyataan Pemilik IPTB Bidang Intalasi Tata Udara Gedung</label></td>
        <td><a href="<?php echo base_url('assets/document/surat_iptbTUG/template/11. Surat Pernyataan Pemilik IPTB Bidang Instalasi Tata Udara Gedung.docx');?>" Unduh class="btn btn-success" style="background-color:orange;">Unduh</a></td>
        <td><input type="file" name="file_surat_iptbTUG"></td>
        <td><input class="btn btn-success" type="submit" value="upload" /></td>
        </form>
      </tr>
      <tr>
        <?php echo form_open_multipart('dokumen/upload_surat_iptbTDG');?>
        <td><label class="">Surat Pernyataan Pemilik IPTB Bidang Instalasi Transportasi Dalam Gedung</label></td>
        <td><a href="<?php echo base_url('assets/document/surat_iptbTDG/template/12. Surat Pernyatan Pemilik IPTB Bidang Instalasi Transportasi Dalam Gedung.docx');?>" Unduh class="btn btn-success" style="background-color:orange;">Unduh</a></td>
        <td><input type="file" name="file_surat_iptbTDG"></td>
        <td><input class="btn btn-success" type="submit" value="upload" /></td>
        </form>
      </tr>
      <tr>
        <?php echo form_open_multipart('dokumen/upload_surat_SRAH');?>
        <td><label class="">Surat Peryataan Sumur Resapan Air Hujan (SRAH)</label></td>
        <td><a href="<?php echo base_url('assets/document/surat_SRAH/template/13. Surat Pernyataan Telah Membuat Sumur Resapan Air Hujan (SRAH).docx');?>" Unduh class="btn btn-success" style="background-color:orange;">Unduh</a></td>
        <td><input type="file" name="file_surat_SRAH"></td>
        <td><input class="btn btn-success" type="submit" value="upload" /></td>
        </form>
      </tr>
      <tr>
        <?php echo form_open_multipart('dokumen/upload_iptbArsitek');?>
        <td><label class=""> Legalisir IPTB Bidang Arsitektur</label></td>
        <td>-</a>
        <td><input type="file" name="file_iptbArsitek"></td>
        <td><input class="btn btn-success" type="submit" value="upload" /></td>
        </form>
      </tr>
      <tr>
        <?php echo form_open_multipart('dokumen/upload_iptbKonstruksi');?>
        <td><label class=""> Legalisir IPTB Bidang Konstruksi</label></td>
        <td>-</a>
        <td><input type="file" name="file_iptbKonstruksi"></td>
        <td><input class="btn btn-success" type="submit" value="upload" /></td>
        </form>
      </tr>

      <tr>
        <?php echo form_open_multipart('dokumen/upload_iptbLAK');?>
        <td><label class=""> Legalisir IPTB Bidang LAK</label></td>
        <td>-</a>
        <td><input type="file" name="file_iptbLAK"></td>
        <td><input class="btn btn-success" type="submit" value="upload" /></td>
        </form>
      </tr>

      <tr>
        <?php echo form_open_multipart('dokumen/upload_iptbLAL');?>
        <td><label class=""> Legalisir IPTB Bidang LAL</label></td>
        <td>-</a>
        <td><input type="file" name="file_iptbLAL"></td>
        <td><input class="btn btn-success" type="submit" value="upload" /></td>
        </form>
      </tr>

      <tr>
        <?php echo form_open_multipart('dokumen/upload_iptbTDG');?>
        <td><label class=""> Legalisir IPTB TDG</label></td>
        <td>-</a>
        <td><input type="file" name="file_iptbTDG"></td>
        <td><input class="btn btn-success" type="submit" value="upload" /></td>
        </form>
      </tr>

      <tr>
        <?php echo form_open_multipart('dokumen/upload_iptbTUG');?>
        <td><label class=""> Legalisir IPTB TUG</label></td>
        <td>-</a>
        <td><input type="file" name="file_iptbTUG"></td>
        <td><input class="btn btn-success" type="submit" value="upload" /></td>
        </form>
      </tr>
      <tr>
        <?php echo form_open_multipart('dokumen/upload_iptbSDP');?>
        <td><label class=""> Legalisir IPTB SDP</label></td>
        <td>-</a>
        <td><input type="file" name="file_iptbSDP"></td>
        <td><input class="btn btn-success" type="submit" value="upload" /></td>
        </form>
      </tr>

      <tr>
        <?php echo form_open_multipart('dokumen/upload_surat_koordinatorPngws');?>
        <td><label class="">Surat Pernyataan Koordinator Direksi Pengawas atau Pengkaji Teknik</label></td>
        <td><a href="#" Unduh><button type="submit" name="submit" class="btn btn-success" style="background-color:orange;">Unduh</button></a></td>
        <td><input type="file" name="file_surat_koordinatorPngws"></td>
        <td><input class="btn btn-success" type="submit" value="upload" /></td>
        </form>
      </tr>
      <tr>
        <?php echo form_open_multipart('dokumen/upload_surat_selesaiBangun');?>
        <td><label class="">Surat Pernyataan Selsai Membangun untuk SLF-1</label></td>
        <td><a href="#" Unduh><button type="submit" name="submit" class="btn btn-success" style="background-color:orange;">Unduh</button></a></td>
        <td><input type="file" name="file_surat_selesaiBangun"></td>
        <td><input class="btn btn-success" type="submit" value="upload" /></td>
        </form>
      </tr>
      <tr>
         <?php echo form_open_multipart('dokumen/upload_laporan_kajianTeknis');?>
        <td><label class="">Laporan Direksi Pengawas Untuk SLF-1/Laporan Kajian Teknis dari Pengawas/Pengkaji yang mempunyai Izin Pelaku Teknis BAngunan (IPTB) Sebannyak 3</label></td>
        <td><a href="#" Unduh><button type="submit" name="submit" class="btn btn-success" style="background-color:orange;">Unduh</button></a></td>
        <td><input type="file" name="file_laporan_kajianTeknis"></td>
        <td><input class="btn btn-success" type="submit" value="upload" /></td>
        </form>
      </tr>
      <tr>
       <?php echo form_open_multipart('dokumen/upload_surat_bayarRetribusi');?>
        <td><label class=""></label>Surat Pernyaataan Kesanggupan Pembayaran Retribusi</td>
        <td><a href="#" Unduh><button type="submit" name="submit" class="btn btn-success" style="background-color:orange;">Unduh</button></a></td>
        <td><input type="file" name="file_surat_bayarRetribusi"></td>
        <td><input class="btn btn-success" type="submit" value="upload" /></td>
        </form>
      </tr>
      <tr>
        <?php echo form_open_multipart('dokumen/upload_surat_pemilikBangunan');?>
        <td><label class=""></label>Surat Pernyataan Pemilik Bangunan</td>
        <td><a href="#" Unduh><button type="submit" name="submit" class="btn btn-success" style="background-color:orange;">Unduh</button></a></td>
        <td><input type="file" name="file_surat_pemilikBangunan"></td>
        <td><input class="btn btn-success" type="submit" value="upload" /></td>
        </form>
      </tr>
      <tr>
         <?php echo form_open_multipart('dokumen/upload_rekomendasi_DAMKAR');?>
        <td><label class="">Rekomendasi DAMKAR</label></td>
        <td></td>
        <td><input type="file" name="file_rekomendasi_DAMKAR"></td>
        <td><input class="btn btn-success" type="submit" value="upload" /></td>
        </form>
      </tr>

      <tr>
       <?php echo form_open_multipart('dokumen/upload_rekomendasi_biroPKLH');?>
        <td><label class="">Rekomendasi Biro PKLH</label></td>
        <td></td>
        <td><input type="file" name="file_rekomendasi_biroPKLH"></td>
        <td><input class="btn btn-success" type="submit" value="upload" /></td>
        </form>
      </tr>

      <tr>
        <?php echo form_open_multipart('dokumen/upload_rekomendasi_DLH');?>
        <td><label class="">Rekomendasi DLH</label></td>
        <td></td>
        <td><input type="file" name="file_rekomendasi_DLH"></td>
        <td><input class="btn btn-success" type="submit" value="upload" /></td>
        </form>
      </tr>

      <tr>
        <?php echo form_open_multipart('dokumen/upload_rekomendasi_BPRD');?>
        <td><label class="">Rekomendasi BPRD</label></td>
        <td></td>
        <td><input type="file" name="file_rekomendasi_BPRD"></td>
        <td><input class="btn btn-success" type="submit" value="upload" /></td>
        </form>
      </tr>

      <tr>
        <?php echo form_open_multipart('dokumen/upload_rekomendasi_biroHukum');?>
        <td><label class="">Rekomendasi Biro Hukum</label></td>
        <td></td>
        <td><input type="file" name="file_rekomendasi_biroHukum"></td>
        <td><input class="btn btn-success" type="submit" value="upload" /></td>
        </form>
      </tr>

      <tr>
        <?php echo form_open_multipart('dokumen/upload_rekomendasi_BPAD');?>
        <td><label class="">Rekomendasi BPAD (Aset)</label></td>
        <td></td>
        <td><input type="file" name="file_rekomendasi_BPAD"></td>
        <td><input class="btn btn-success" type="submit" value="upload" /></td>
        </form>
      </tr>

      <tr>
      <?php echo form_open_multipart('dokumen/upload_rekomendasi_dinasTK');?>
        <td><label class="">Rekomendasi Dinas Tenaga Kerja</label></td>
        <td></td>
        <td><input type="file" name="file_rekomendasi_dinasTK"></td>
        <td><input class="btn btn-success" type="submit" value="upload" /></td>
        </form>
      </tr>

      <tr>
      <?php echo form_open_multipart('dokumen/upload_rekomendasi_dinasBM');?>
        <td><label class="">Rekomendasi Dinas Bina Marga</label></td>
        <td></td>
        <td><input type="file" name="file_rekomendasi_dinasBM"></td>
        <td><input class="btn btn-success" type="submit" value="upload" /></td>
        </form>
      </tr>

      <tr>
        <?php echo form_open_multipart('dokumen/upload_rekomendasi_SKPDlain');?>
        <td><label class="">Rekomendasi SKPD Lainnya</label></td>
        <td></td>
        <td><input type="file" name="file_rekomendasi_SKPDlain"></td>
        <td><input class="btn btn-success" type="submit" value="upload" /></td>
        </form>
      </tr>
      <tr>
        <?php echo form_open_multipart('dokumen/upload_rekomendasi_rapimGubernur');?>
        <td><label class="">Rekomendasi Rapim Gubernur / BKPRD</label></td>
        <td></td>
        <td><input type="file" name="file_rekomendasi_rapimGubernur"></td>
        <td><input class="btn btn-success" type="submit" value="upload" /></td>
        </form>
      </tr>

    </tbody>

  </table>
  <button type="submit" class = "btn btn-success" >Simpan</button>
</div>
