<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div class="kt-subheader   kt-grid__item" id="kt_subheader">

</div>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">


  <div class="card mb-3">
        <div class="card-header">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item active" aria-current="page">Daftar Verifikasi</li>
            </ol>
          </nav>
        </div>
        <div class="card-body">
                             <?php

                             //$this->load->library('Gate_keeper');

							// print_r($this->session->userdata('unm')); ?>
          <div class="table-responsive">
          <table id="m_artikel" class="table table-striped table-bordered">
              <thead>
              <tr>
                  <th>Nama Pemohon</th>
                  <th>Nama Gedung / Kawasan</th>
                  <th>Nama Tower</th>
                  <th>Jenis Permohonan</th>
                  <th>Tanggal Pengajuan</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>

                <tr>
                  <th>Agus Martono</th>
                  <th>Emeral City</th>
                  <th>Tower 1A</th>
                  <th>SLF Baru</th>
                  <th>20/18/2019</th>

                  <th><a href="<?= base_url('verifikasi');?>"><button type="submit" name="submit" class="btn btn-success" style="background-color:green;"> <i class="fa fa-check-square-o"></i></button></a>
                     </button>
                     <button type="submit" name="submit" class="btn btn-info"> <i class="fa fa-comment"></i></button></th>
                </tr>
                <tr>
                  <th>Bagus Hartanto</th>
                  <th>Green Tower</th>
                  <th>Tower 7S</th>
                  <th>Perpanjang SLF</th>
                  <th>13/09/2019</th>


                  <th><a href="<?= base_url('verifikasi');?>"><button type="submit" name="submit" class="btn btn-success" style="background-color:green;"> <i class="fa fa-check-square-o"></i></button></a>
                     </button>
                     <button type="submit" name="submit" class="btn btn-info"> <i class="fa fa-comment"></i></button></th>
                </tr>
                <tr>
                  <th>Andika Pambudi</th>
                  <th>Skynet</th>
                  <th>Cyberdine Tower</th>
                  <th>SLF Baru</th>
                  <th>01/01/2019</th>


                  <th><a href="<?= base_url('verifikasi');?>"><button type="submit" name="submit" class="btn btn-success" style="background-color:green;"> <i class="fa fa-check-square-o"></i></button></a>
                     </button>
                     <button type="submit" name="submit" class="btn btn-info"> <i class="fa fa-comment"></i></button></th>
                </tr>

              </tbody>
            </table>
          </div>
        </div>
      </div>
</div>
