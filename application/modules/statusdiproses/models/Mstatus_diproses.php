<?php

class Mstatus_diproses extends CI_Model{
    function getData(){
        $this->db->select('BANGUNAN.NM_BANGUNAN, PENGAJUAN.STATUS_PENGAJUAN, PENGAJUAN_DETAIL.ID_TEKNIS_TOWER');
        $this->db->from('SYSTEM.PENGAJUAN');
        $this->db->join('SYSTEM.BANGUNAN','PENGAJUAN.ID_BANGUNAN = BANGUNAN.ID_BANGUNAN','INNER');
        $this->db->join('SYSTEM.PENGAJUAN_DETAIL','PENGAJUAN.KODE_PENGAJUAN = PENGAJUAN_DETAIL.KODE_PENGAJUAN','INNER');
        $query = $this->db->get();
        if ($query->num_rows())
        {
            return $query->result_array();
        }
        else
        {
            return FALSE;
        }
    }

}