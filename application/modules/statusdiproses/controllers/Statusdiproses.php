<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
        
class Statusdiproses extends MY_Controller {

function __construct()
 	{
 		parent::__construct();
 		$this->load->model('Mstatus_diproses');
 		$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
 		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
 		$this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
 		$this->output->set_header('Pragma: no-cache');
 	}

	public function index()
	{

		$data['group_menu'] = 'hr';
		$data['content']    = 'statusdiproses';
		$data['Datadiproses'] = $this->Mstatus_diproses->getData();
		// print_r($data['Datadiproses']);
		// exit;
		
		$this->load->view('base/index', $data);
	}
	
}