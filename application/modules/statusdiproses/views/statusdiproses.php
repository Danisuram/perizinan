<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

</head>

<div class="kt-subheader   kt-grid__item" id="kt_subheader">

</div>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">


  <div class="card mb-3">
        <div class="card-header">
          <h3>Status Diproses</h3>
        </div>
        <div class="card-body">

          <div class="table-responsive">
          <table id="m_artikel" class="table table-striped table-bordered">
              <thead>
              <tr>
                  <th>Nama Gedung / Kawasan</th>
                  <th>ID Tower</th>
                  <th>Status Permohonan</th>
                  <th>Aksi</th>
                  <!-- <th>Lengkapi Berkas</th> -->
                </tr>
              </thead>
              <tbody>

              <?php foreach ($Datadiproses as $i):?>
                <tr>
                  <td><?php echo $i['NM_BANGUNAN']?></td>
                  <td><?php echo $i['ID_TEKNIS_TOWER']?></td>
                  <td><?php echo $i['STATUS_PENGAJUAN']?></td>         
                  <!-- <td><a href="<?php echo base_url('detail_bangunan');?>"><button class="btn btn-info" style="background-color: green;"><i class="fa fa-edit"></i></button></a> | <button class="btn btn-info" style="background-color: red;"><i class="fas fa-trash"></i></button> </td> -->
                  <td><a href="<?php echo base_url('dokumen');?>"><button class="btn btn-success" style="background-color: green;">Lengkapi Berkas <i class="fa fa-arrow-right"></i></button></a></td>
                </tr>
              <?php endforeach;?>
              </tbody>
            </table>
          </div>
        </div>
      </div>

