<link href="<?php echo base_url('assets/font-awesome/font-awesome.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/bootstrap4/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url('assets/bootstrap4/css/bootstrap.css'); ?>" rel="stylesheet" type="text/css" />
   <link href="<?php //echo base_url('assets/build/css/custom.min.css')?>" rel="stylesheet">

<style>
.widget-area.blank {
background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
-webkit-box-shadow: none;
-moz-box-shadow: none;
-ms-box-shadow: none;
-o-box-shadow: none;
box-shadow: none;
}
body .no-padding {
padding: 0;
}
.widget-area {
background-color: #fff;
-webkit-border-radius: 4px;
-moz-border-radius: 4px;
-ms-border-radius: 4px;
-o-border-radius: 4px;
border-radius: 4px;
-webkit-box-shadow: 0 0 16px rgba(0, 0, 0, 0.05);
-moz-box-shadow: 0 0 16px rgba(0, 0, 0, 0.05);
-ms-box-shadow: 0 0 16px rgba(0, 0, 0, 0.05);
-o-box-shadow: 0 0 16px rgba(0, 0, 0, 0.05);
box-shadow: 0 0 16px rgba(0, 0, 0, 0.05);
float: left;
margin-top: 30px;
padding: 25px 30px;
position: relative;
width: 100%;
}
.status-upload {
background: none repeat scroll 0 0 #f5f5f5;
-webkit-border-radius: 4px;
-moz-border-radius: 4px;
-ms-border-radius: 4px;
-o-border-radius: 4px;
border-radius: 4px;
float: left;
width: 100%;
}
.status-upload form {
float: left;
width: 100%;
}
.status-upload form textarea {
background: none repeat scroll 0 0 #fff;
border: medium none;
-webkit-border-radius: 4px 4px 0 0;
-moz-border-radius: 4px 4px 0 0;
-ms-border-radius: 4px 4px 0 0;
-o-border-radius: 4px 4px 0 0;
border-radius: 4px 4px 0 0;
color: #777777;
float: left;
font-family: Lato;
font-size: 14px;
height: 142px;
letter-spacing: 0.3px;
padding: 20px;
width: 100%;
resize:vertical;
outline:none;
border: 1px solid #F2F2F2;
}

.status-upload ul {
float: left;
list-style: none outside none;
margin: 0;
padding: 0 0 0 15px;
width: auto;
}
.status-upload ul > li {
float: left;
}
.status-upload ul > li > a {
-webkit-border-radius: 4px;
-moz-border-radius: 4px;
-ms-border-radius: 4px;
-o-border-radius: 4px;
border-radius: 4px;
color: #777777;
float: left;
font-size: 14px;
height: 30px;
line-height: 30px;
margin: 10px 0 10px 10px;
text-align: center;
-webkit-transition: all 0.4s ease 0s;
-moz-transition: all 0.4s ease 0s;
-ms-transition: all 0.4s ease 0s;
-o-transition: all 0.4s ease 0s;
transition: all 0.4s ease 0s;
width: 30px;
cursor: pointer;
}
.status-upload ul > li > a:hover {
background: none repeat scroll 0 0 #606060;
color: #fff;
}
.status-upload form button {
border: medium none;
-webkit-border-radius: 4px;
-moz-border-radius: 4px;
-ms-border-radius: 4px;
-o-border-radius: 4px;
border-radius: 4px;
color: #fff;
float: right;
font-family: Lato;
font-size: 14px;
letter-spacing: 0.3px;
margin-right: 9px;
margin-top: 9px;
padding: 6px 15px;
}
.dropdown > a > span.green:before {
border-left-color: #2dcb73;
}
.status-upload form button > i {
margin-right: 7px;
}

ul.chec-radio {
    margin: 15px;
}
ul.chec-radio li.pz {
    display: inline;
}
.chec-radio label.radio-inline input[type="checkbox"] {
    display: none;
}
.chec-radio label.radio-inline input[type="checkbox"]:checked+div {
    color: #fff;
    background-color: #3ba335;
}
.chec-radio .radio-inline .clab {
    cursor: pointer;
    background: #e7e7e7;
    padding: 7px 20px;
    text-align: center;
    text-transform: uppercase;
    color: "green";
    position: relative;
    height: 34px;
    float: left;
    margin: 0;
}
.chec-radio label.radio-inline input[type="checkbox"]:checked+div:before {
    content: "\e013";
    margin-right: 5px;
    font-family: 'Glyphicons Halflings';
}
.chec-radio label.radio-inline input[type="radio"] {
    display: none;
}
.chec-radio label.radio-inline input[type="radio"]:checked+div {
    color: #fff;
    background-color: #000;
}
.chec-radio label.radio-inline input[type="radio"]:checked+div:before {
    content: "\e013";
    margin-right: 5px;
    font-family: 'Glyphicons Halflings';
}

.panel.with-nav-tabs .panel-heading{
    padding: 5px 5px 0 5px;
}
.panel.with-nav-tabs .nav-tabs{
	border-bottom: none;
}
.panel.with-nav-tabs .nav-justified{
	margin-bottom: -1px;
}
/********************************************************************/
/*** PANEL DEFAULT ***/
.with-nav-tabs.panel-default .nav-tabs > li > a,
.with-nav-tabs.panel-default .nav-tabs > li > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li > a:focus {
    color: #777;
}
.with-nav-tabs.panel-default .nav-tabs > .open > a,
.with-nav-tabs.panel-default .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-default .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-default .nav-tabs > li > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li > a:focus {
    color: red;
	background-color: #ddd;
	border-color: transparent;
}
.with-nav-tabs.panel-default .nav-tabs > li.active > a,
.with-nav-tabs.panel-default .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li.active > a:focus {
	color: #555;
	background-color: #fff;
	border-color: #ddd;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #f5f5f5;
    border-color: #ddd;
}
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #777;
}
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #ddd;
}
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-default .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #555;
}
/********************************************************************/
/*** PANEL PRIMARY ***/
.with-nav-tabs.panel-primary .nav-tabs > li > a,
.with-nav-tabs.panel-primary .nav-tabs > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
    color: #fff;
}
.with-nav-tabs.panel-primary .nav-tabs > .open > a,
.with-nav-tabs.panel-primary .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-primary .nav-tabs > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
	color: #fff;
	background-color: #3071a9;
	border-color: transparent;
}
.with-nav-tabs.panel-primary .nav-tabs > li.active > a,
.with-nav-tabs.panel-primary .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.active > a:focus {
	color: #428bca;
	background-color: #fff;
	border-color: #428bca;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #428bca;
    border-color: #3071a9;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #fff;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #3071a9;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    background-color: #4a9fe9;
}
/********************************************************************/
/*** PANEL SUCCESS ***/
.with-nav-tabs.panel-success .nav-tabs > li > a,
.with-nav-tabs.panel-success .nav-tabs > li > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li > a:focus {
	color: #3c763d;
}
.with-nav-tabs.panel-success .nav-tabs > .open > a,
.with-nav-tabs.panel-success .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-success .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-success .nav-tabs > li > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li > a:focus {
	color: #3c763d;
	background-color: #d6e9c6;
	border-color: transparent;
}
.with-nav-tabs.panel-success .nav-tabs > li.active > a,
.with-nav-tabs.panel-success .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li.active > a:focus {
	color: #3c763d;
	background-color: #fff;
	border-color: #d6e9c6;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #dff0d8;
    border-color: #d6e9c6;
}
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #3c763d;
}
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #d6e9c6;
}
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-success .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #3c763d;
}
/********************************************************************/
/*** PANEL INFO ***/
.with-nav-tabs.panel-info .nav-tabs > li > a,
.with-nav-tabs.panel-info .nav-tabs > li > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li > a:focus {
	color: #31708f;
}
.with-nav-tabs.panel-info .nav-tabs > .open > a,
.with-nav-tabs.panel-info .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-info .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-info .nav-tabs > li > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li > a:focus {
	color: #31708f;
	background-color: #bce8f1;
	border-color: transparent;
}
.with-nav-tabs.panel-info .nav-tabs > li.active > a,
.with-nav-tabs.panel-info .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li.active > a:focus {
	color: #31708f;
	background-color: #fff;
	border-color: #bce8f1;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #d9edf7;
    border-color: #bce8f1;
}
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #31708f;
}
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #bce8f1;
}
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-info .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #31708f;
}
/********************************************************************/
/*** PANEL WARNING ***/
.with-nav-tabs.panel-warning .nav-tabs > li > a,
.with-nav-tabs.panel-warning .nav-tabs > li > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li > a:focus {
	color: #8a6d3b;
}
.with-nav-tabs.panel-warning .nav-tabs > .open > a,
.with-nav-tabs.panel-warning .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-warning .nav-tabs > li > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li > a:focus {
	color: #8a6d3b;
	background-color: #faebcc;
	border-color: transparent;
}
.with-nav-tabs.panel-warning .nav-tabs > li.active > a,
.with-nav-tabs.panel-warning .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li.active > a:focus {
	color: #8a6d3b;
	background-color: #fff;
	border-color: #faebcc;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #fcf8e3;
    border-color: #faebcc;
}
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #8a6d3b;
}
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #faebcc;
}
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-warning .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff;
    background-color: #8a6d3b;
}
/********************************************************************/
/*** PANEL DANGER ***/
.with-nav-tabs.panel-danger .nav-tabs > li > a,
.with-nav-tabs.panel-danger .nav-tabs > li > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li > a:focus {
	color: #a94442;
}
.with-nav-tabs.panel-danger .nav-tabs > .open > a,
.with-nav-tabs.panel-danger .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-danger .nav-tabs > li > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li > a:focus {
	color: #a94442;
	background-color: #ebccd1;
	border-color: transparent;
}
.with-nav-tabs.panel-danger .nav-tabs > li.active > a,
.with-nav-tabs.panel-danger .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li.active > a:focus {
	color: #a94442;
	background-color: #fff;
	border-color: #ebccd1;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #f2dede; /* bg color */
    border-color: #ebccd1; /* border color */
}
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #a94442; /* normal text color */
}
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #ebccd1; /* hover bg color */
}
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-danger .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    color: #fff; /* active text color */
    background-color: #a94442; /* active bg color */
}

  .bg-modal{
    width:100%;
    height:100%;


    position: absolute;
    top:0;
    display: flex;
    justify-content: center;
    align-items: center;
    display: none;


  }
  .modal-content{
    width: 500px;
    height:300px;
    background-color: white;
    border-radius: 4px;
    text-align: center;
    padding: 20px;
    position: relative;
  }
  .input{
    width:50px;
    display: block;
    margin: 15px auto;
  }
  .close{
    position: absolute;
    top: 0;
    right: 14px;
    font-size: 42px;
    transform: rotate(45deg):
    cursor: pointer;
  }
</style>
<div class="kt-subheader   kt-grid__item" id="kt_subheader">

</div>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

<div class="m-grid-col-md-12">

    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="naskah_dinas">Daftar Verifikasi</a></li>
        <li class="breadcrumb-item active" aria-current="page">Detail Bangunan</li>
      </ol>
    </nav>

		<div class="form-horizontal form-label-left">
			<br />

     <div class="m-grid-col-md-12">
                <div class="portlet-body">
                
  
				  
                            <ul class="nav nav-pills nav-justified breadcrumb">
                                <li class="active nav-item"><a class="nav-link" href="#tab1default" data-toggle="tab">Data Bangunan</a></li>
                                <li class="nav-item"><a class="nav-link"  href="#tab2default" data-toggle="tab">Data Tanah</a></li>
                                <li class="nav-item"><a class="nav-link" href="#tab3default" data-toggle="tab">Data SIPPT</a></li>
                                <li class="nav-item"><a class="nav-link" href="#tab4default" data-toggle="tab">IPTB</a></li>
                                <li class="nav-item"><a class="nav-link" href="#tab5default" data-toggle="tab">Data Sumur Resapan</a></li>
                                <li class="nav-item"><a class="nav-link" href="#tab6default" data-toggle="tab">Data Perizinan Lain Terkait</a></li>
                                <li class="nav-item"><a class="nav-link" href="#tab7default" data-toggle="tab">Data Teknis Terkait</a></li>
                                <li class="nav-item"><a class="nav-link" href="#tab8default" data-toggle="tab">Data Perizinan IMB Yang Pernah Terbit</a></li>
                                <li class="nav-item"><a class="nav-link" href="#tab9default" data-toggle="tab">Data Perizinan SLF Yang Pernah Terbit</a></li>
                            </ul>
							
							
              
                    
                      <div class="tab-content">
                            <div class="tab-pane fade in active" id="tab1default">
                              <div id="data_bangunan">
							  
							<!--     <div class="form-group row row">
								<label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
								<div class="col-sm-3">
								  <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="email@example.com">
								</div>
							  </div>
							  <div class="form-group row row">
								<label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
								<div class="col-sm-3">
								  <input type="password" class="form-control" id="inputPassword" placeholder="Password">
								</div>
							  </div>-->

                    							<div class="form-group row row">
                    								<label class="control-label col-md-3 col-xs-12">Nama Bangunan</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    									<select disabled class="form-control" name="nama_bangunan" id="nama_bangunan" >
                    										<option value="conclave_simatupang">Conclave Simaptupang</option>
                    									</select>
                    								</div>
                    							</div>
                    							<div class="form-group row row">
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Fungsi Utama</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    									<input type="text" class="form-control" name="fungsi_utama" placeholder="Fungsi Utama" readonly="">
                    								</div>

                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Fungsi Tambahan</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    									<input type="text" class="form-control" name="fungsi_tambahan" placeholder="Fungsi Tambahan"readonly="">
                    								</div>


                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Jenis Bangunan</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    									<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Jenis Bangunan"readonly="">
                    								</div>




                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor Pendaftaran Bangunan</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    									<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Nomor Pendaftaran Bangunan"readonly="">
                    								</div>

                    							</div>
                    							<div class="form-group row row">
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Alamat Bangunan</label>
                    								<div class="col-md-8 col-sm-8 col-xs-12">
                    									<input type="text" class="form-control" id="alamat_now" name="ALAMAT_SEKARANG" placeholder="Alamat Bangunan" readonly="" >
                    								</div>

                    							</div>

                    							<div class="form-group row row">
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                    								<div class="col-md-8 col-sm-8 col-xs-12">
                    									<select disabled class="form-control" name="PROVINSI_SEKARANG" id="prov2" >
                    									<option value="">-- Provinsi --</option>
                    									</select>
                    								</div>
                    							</div>

                    							<div class="form-group row row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                    							<div class="col-md-2 col-sm-2 col-xs-12">
                    								<select disabled class="form-control" name="KECAMATAN_SEKARANG" id="kecamatan2" >
                    								<option value="">-- Kecamatan --</option>
                    								</select>

                    							</div>

                    							<label class="control-label col-md-1 col-sm-1 col-xs-12"></label>
                    							<div class="col-md-2 col-sm-2 col-xs-12">
                    								<select disabled class="form-control" name="KELURAHAN_SEKARANG" id="kelurahan2" >
                    								<option value="">-- Kelurahan --</option>
                    								</select>

                    							</div>

                    							<label class="control-label col-md-1 col-sm-1 col-xs-12"></label>
                    							<div class="col-md-2 col-sm-2 col-xs-12">
                    								<select disabled class="form-control" name="KOTA_SEKARANG" id="kota2" >
                    								<option value="">-- Kota --</option>
                    								</select>

                    							</div>


                                  <div class="form-group row row">
                                    <br/>
                                    <label class="control-label col-md-1 col-sm-1 col-xs-12"></label>

                                    <button type="submit" name="submit" id="btnComment" class="btn btn-info" >Tambah Komentar</button>


                                  </div>
                    							</div>
                    					</div>
                            </div>
                            <div class="tab-pane fade in " id="tab2default">
                              <div class="form-horizontal form-label-left" id="data_tanah">
                    						<div class="form-group row">
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Jumlah Sertifikat</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    									<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Jumlah Sertifikat" readonly="">
                    								</div>
                    							</div>
                    							<div class="form-group row">
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Sertifikat 1</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    									<a href="#" download>Sertifikat 1.jpg</a>
                    								</div>

                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor Sertifikat</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    									<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Nomor Sertifikat" readonly="">
                    								</div>

                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Luas Tanah</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    									<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Luas Tanah" readonly="">
                    								</div>
                    							</div>
                    							<div class="form-group row">
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Sertifikat 2</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    									<a href="#" download>Sertifikat 2.jpg</a>
                    								</div>
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor Sertifikat</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    									<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Nomor Sertifikat" readonly="">
                    								</div>
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Luas Tanah</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    									<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Luas Tanah" readonly="">
                    								</div>
                    							</div>
                    							<div class="form-group row">
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Sertifikat 3</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    									<a href="#" download>Sertifikat 3.jpg</a>
                    								</div>

                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor Sertifikat</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    									<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Nomor Sertifikat" readonly="">
                    								</div>
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Luas Tanah</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    									<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Luas Tanah" readonly="">
                    								</div>
                    							</div>
                    							<div class="form-group row">
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12"> Total Luas Tanah</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    									<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Total Luas Tanah" readonly="">
                    								</div>
                    							</div>

                    							<div class="form-group row">
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Jumlah Objek PBB</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    									<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Jumbal Objek PBB" readonly="">
                    								</div>
                    							</div>
                    							<div class="form-group row">
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Objek Pajak PBB 1</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    									<a href="#" download>Objek Pajak PBB 1.jpg</a>
                    								</div>
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor Objek Pajak PBB 1</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    									<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Nomor Objek Pajak PBB" readonly="">
                    								</div>
                    							</div>
                    							<div class="form-group row">
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Luas Bumi</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    									<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Luas Bumi" readonly="">
                    								</div>
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Luas Bangunan</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    									<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Luas Bangunan" readonly="">
                    								</div>
                    							</div>
                    							<div class="form-group row">
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Objek Pajak PBB 2</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">

                    									<a href="#" download>Objek Pajak PBB 2.jpg</a>
                    								</div>
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor Objek Pajak PBB 2</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    									<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Nomor Objek Pajak PBB" readonly="">
                    								</div>
                    							</div>
                    							<div class="form-group row">
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Luas Bumi</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    									<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Luas Bumi" readonly="">
                    								</div>
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Luas Bangunan</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    									<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Luas Bangunan" readonly="">
                    								</div>
                    							</div>
                    							<div class="form-group row">
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Objek Pajak PBB 3</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">

                    									<a href="#" download>Objek Pajak PBB 3.jpg</a>
                    								</div>
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor Objek Pajak PBB 3</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    									<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Nomor Objek Pajak PBB" readonly="">
                    								</div>
                    							</div>
                    							<div class="form-group row">
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Luas Bumi</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    									<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Luas Bumi" readonly="">
                    								</div>
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Luas Bangunan</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    									<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Luas Bangunan" readonly="">
                    								</div>
                    							</div>
                    							<div class="form-group row">
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Luas Total Bumi Bersama Dalam PBB</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    									<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Luas Tanah" readonly="">
                    								</div>
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Luas Total Bangunan Bersama Dalam PBB</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    									<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Luas Tanah" readonly="">
                    								</div>
                    							</div>
                    							<div class="form-group row">
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Surat Perjanjian Dengan Pengelola</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">

                    									<a href="#" download>Surat Perjanjian Dengan Pengelola.pdf</a>
                    								</div>
                    							</div>
                                  <div class="form-group row">
                                    <br/>
                                    <label class="control-label col-md-1 col-sm-1 col-xs-12"></label>

                                    <button type="submit" name="submit" id="btnComment" class="btn btn-info" >Tambah Komentar</button>


                                  </div>
                    					</div>
                            </div>
                            <div class="tab-pane fade in " id="tab3default">
                              <div class="form-horizontal form-label-left" id="data_sippt">
                  								<div class="form-group row">
                  									<label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor SIPPT/IPPTI/IPPTR</label>
                  									<div class="col-md-3 col-sm-3 col-xs-12">
                  										<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Nomor SIPPT/IPPTI/IPPTR" readonly="">
                  									</div>
                  									</div>
                  									<div class="form-group row">
                  									<label class="control-label col-md-3 col-sm-3 col-xs-12">Luas Lahan</label>
                  									<div class="col-md-3 col-sm-3 col-xs-12">
                  										<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Luas Lahan" readonly="">
                  									</div>
                  									</div>
                  									<div class="form-group row">
                  									<label class="control-label col-md-3 col-sm-3 col-xs-12">Luas Lahan Yang Belum Terbangun</label>
                  									<div class="col-md-3 col-sm-3 col-xs-12">
                  										<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Luas Lahan Yang Belum Terbangun" readonly="">
                  									</div>
                  									</div>

                  									<div class="form-group row">
                  									<label class="control-label col-md-3 col-sm-3 col-xs-12">Jumlah Bangunan Terbangun Dalam Area</label>
                  									<div class="col-md-3 col-sm-3 col-xs-12">
                  										<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Jumlah Bangunan Terbangun" readonly="">
                  									</div>
                  									</div>

                  									<div class="form-group row">
                  									<label class="control-label col-md-3 col-sm-3 col-xs-12"> Sanksi</label>
                  									<div class="col-md-3 col-sm-3 col-xs-12">
                  										<select disabled class="form-control" name="PROVINSI" id="prov" >
                  										<option value="">Ada</option>
                  										<option value="dirut">Tidak Ada</option>
                  										</select>
                  									</div>
                  									</div>

                  									<div class="form-group row">
                  									<label class="control-label col-md-3 col-sm-3 col-xs-12">Sanksi 1</label>
                  									<div class="col-md-3 col-sm-3 col-xs-12">
                  										<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Sanksi" readonly="">
                  									</div>
                  									</div>
                  									<div class="form-group row">
                  									<label class="control-label col-md-3 col-sm-3 col-xs-12">Sanksi 2</label>
                  									<div class="col-md-3 col-sm-3 col-xs-12">
                  										<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Sanksi" readonly="">
                  									</div>
                  									</div>
                  									<div class="form-group row">
                  									<label class="control-label col-md-3 col-sm-3 col-xs-12">Sanksi 3</label>
                  									<div class="col-md-3 col-sm-3 col-xs-12">
                  										<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Sanksi" readonly="">
                  									</div>
                  									</div>

                  									<div class="form-group row">
                  									<label class="control-label col-md-3 col-sm-3 col-xs-12">Total Luas Kewajiban Berupa Lahan</label>
                  									<div class="col-md-3 col-sm-3 col-xs-12">
                  										<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Total Luas Kewajiban" readonly="">
                  									</div>
                  									<label class="control-label col-md-3 col-sm-3 col-xs-12">Surat Perjanjian Pemenuhan Kewajibab (SPPK)</label>
                  									<div class="col-md-3 col-sm-3 col-xs-12">
                  										<a href="#" download> Surat Perjanjian Pemenuhan Kewajiban (SPPK).pdf</a>
                  									</div>
                  									<label class="control-label col-md-3 col-sm-3 col-xs-12">BAPF Kewajiban</label>
                  									<div class="col-md-3 col-sm-3 col-xs-12">
                  										<a href="#" download> BAPF Kewajiban.pdf</a>
                  									</div>
                  									<label class="control-label col-md-3 col-sm-3 col-xs-12">BAST Kewajiban</label>
                  									<div class="col-md-3 col-sm-3 col-xs-12">
                  									<a href="#" download> BAST.pdf</a>
                  									</div>
                  									<label class="control-label col-md-3 col-sm-3 col-xs-12">Total Luas Kewajiban Yang Belum Diserahkan</label>
                  									<div class="col-md-3 col-sm-3 col-xs-12">
                  										<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Total Luas Kewajiban yang belum diserahkan" readonly="">
                  									</div>
                  									</div>

                  									<div class="form-group row">
                  									<label class="control-label col-md-3 col-sm-3 col-xs-12">Pemenuhan Kewajiban SIPPT, S3PL dan Sanksi Lain</label>
                  									<div class="col-md-3 col-sm-3 col-xs-12">
                  										<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Pemenuhan Kewajiban" readonly="">
                  									</div>
                  									</div>

                  									<div class="form-group row">
                  									<label class="control-label col-md-3 col-sm-3 col-xs-12">Bulan</label>
                  									<div class="col-md-3 col-sm-3 col-xs-12">
                  										<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Bulan" readonly="">
                  									</div>
                  									<label class="control-label col-md-3 col-sm-3 col-xs-12">Tahun</label>
                  									<div class="col-md-3 col-sm-3 col-xs-12">
                  										<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Bulan" readonly="">
                  									</div>
                                    <div class="form-group row">
                                      <br/>
                                      <label class="control-label col-md-1 col-sm-1 col-xs-12"></label>

                                      <button type="submit" name="submit" id="btnComment" class="btn btn-info" >Tambah Komentar</button>


                                    </div>
                  									</div>
                  						</div>
                            </div>
                            <div class="tab-pane fade in " id="tab4default">
                              <div class="form-horizontal form-label-left" id="iptb_arsitek">
                    							<div class="form-group row">
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Nama IPTB</label>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                      <!-- <input type="number" class="form-control" name="NAMA_IPTB" placeholder="Nama IPTB Arsitektur" readonly=""> -->

                                        <select class="form-control" name="PROVINSI" id="prov" >
                                          <option value="">IPTB Arsitektur</option>
                                          <option value="">IPTB Konstruksi</option>
                                          <option value="">IPTB Bidang Instalasi Listrik Arus Lemah (LAL)</option>
                                          <option value="">IPTB Bidang Instalasi Listrik Arus Kuat (LAK)</option>
                                          <option value="">IPTB Bidang Instalasi Sanitasi Drainase Pemipaan</option>
                                          <option value="">IPTB Bidang Instalasi Tata Udara Gedung</option>
                                          <option value="">IPTB Bidang Instalasi Transportasi dalam Gedung</option>
                                        </select>
                                    </div>

                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Perusahaan</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    									<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Nama Perusahaan" readonly="">
                    								</div>
                    								</div>

                    								<div class="form-group row">
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor IPTB Arsitektur</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    									<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Nama IPTB" readonly="">
                    								</div>

                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Jenis IPTB</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    									<select disabled class="form-control" name="Jenis_IPTB">

                    									<option value="dirut">IPTB A</option>

                    									</select>
                    								</div>
                    								</div>

                    								<div class="form-group row">
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor KTP</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    									<input type="number" class="form-control" name="NO_KTP" placeholder="Nomor KTP" readonly="">
                    								</div>

                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor NPWP</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="number" class="form-control" name="NO_NPWP" placeholder="Nomor NPWP" readonly="">
                    								</div>
                    								</div>




                    								<div class="form-group row">
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Alamat Sesuai KTP</label>
                    								<div class="col-md-9 col-sm-9 col-xs-12">
                    									<input type="text" class="form-control" id="alamat_ktp" name="ALAMAT_KTP" placeholder="Alamat Sesuai KTP" readonly="">
                    								</div>
                    								</div>

                    								<div class="form-group row">
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                    								<div class="col-md-8 col-sm-8 col-xs-12">
                    									<select disabled class="form-control" name="PROVINSI" id="prov" >
                    									<option value="">-- Provinsi --</option>
                    									</select>
                    								</div>
                    								</div>

                    								<div class="form-group row">
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                    								<div class="col-md-2 col-sm-2 col-xs-12">
                    									<select disabled class="form-control" name="KECAMATAN" id="kecamatan" >
                    									<option value="">-- Kecamatan --</option>
                    									</select>
                    								</div>

                    								<label class="control-label col-md-1 col-sm-1 col-xs-12"></label>
                    								<div class="col-md-2 col-sm-2 col-xs-12">
                    									<select disabled class="form-control" name="KELURAHAN" id="kelurahan" >
                    									<option value="">-- Kelurahan --</option>
                    									</select>

                    								</div>

                    								<label class="control-label col-md-1 col-sm-1 col-xs-12"></label>
                    								<div class="col-md-2 col-sm-2 col-xs-12">
                    									<select disabled class="form-control" name="KOTA" id="kota" >
                    									<option value="">-- Kota --</option>

                    								</div>
                    								</div>

                    								<div class="form-group row">
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Alamat Tempat Tinggal Sekarang</label>
                    								<div class="col-md-8 col-sm-8 col-xs-12">
                    									<input type="text" class="form-control" id="alamat_now" name="ALAMAT_SEKARANG" placeholder="Alamat Sesuai KTP" readonly="" >
                    								</div>
                    								</div>

                    								<div class="form-group row">
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                    								<div class="col-md-8 col-sm-8 col-xs-12">
                    									<select disabled class="form-control" name="PROVINSI_SEKARANG" id="prov2" >
                    									<option value="">-- Provinsi --</option>
                    									</select>
                    								</div>
                    								</div>

                    								<div class="form-group row">
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                    								<div class="col-md-2 col-sm-2 col-xs-12">
                    									<select disabled class="form-control" name="KECAMATAN_SEKARANG" id="kecamatan2" >
                    									<option value="">-- Kecamatan --</option>
                    									</select>
                    								</div>

                    								<label class="control-label col-md-1 col-sm-1 col-xs-12"></label>
                    								<div class="col-md-2 col-sm-2 col-xs-12">
                    									<select disabled class="form-control" name="KELURAHAN_SEKARANG" id="kelurahan2" >
                    									<option value="">-- Kelurahan --</option>
                    									</select>

                    								</div>

                    								<label class="control-label col-md-1 col-sm-1 col-xs-12"></label>
                    								<div class="col-md-2 col-sm-2 col-xs-12">
                    									<select disabled class="form-control" name="KOTA_SEKARANG" id="kota2" >
                    									<option value="">-- Kota --</option>
                    									</select>

                    								</div>
                    								</div>

                    								<div class="form-group row">
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor Telepon</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    									<input type="number" class="form-control" name="NO_TELP" placeholder="Nomor Telepon" readonly="">
                    								</div>

                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Alamat email</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    									<input type="email" class="form-control" name="EMAIL" placeholder="Alamat email" readonly="" >
                    								</div>
                    								</div>
                    								<div class="form-group row">


                    								<label class="control-label col-md-3 col-sm-3 col-xs-12">Legalisir Surat Izin</label>
                    								<div class="col-md-3 col-sm-3 col-xs-12">

                    								<a href="#" download>Legalisir Surat Izin.pdf</a>
                    								</div>
                                    <div class="form-group row">
                                      <br/>
                                      <label class="control-label col-md-1 col-sm-1 col-xs-12"></label>

                                      <button type="submit" name="submit" id="btnComment" class="btn btn-info" >Tambah Komentar</button>


                                    </div>
                    							</div>
                    					</div>
                            </div>
                            <div class="tab-pane fade in " id="tab5default">
                              <div id="data_sumur">
                    								<div class="form-group row">
                    									<label class="control-label col-md-3 col-sm-3 col-xs-12">Surat Pernyataan Sumur Resapan Air Hujan (SRAH)</label>
                    									<div class="col-md-3 col-sm-3 col-xs-12">
                    										<select disabled class="form-control" name="Jenis_IPTB">
                    										<option value="">Ada</option>
                    										<option value="dirut">Tidak Ada</option>
                    										</select>
                    									</div>
                    									<div class="col-md-3 col-sm-3 col-xs-12">
                    									<a href="#" download>Surat Pernyataan Sumur Resapan Air Hujan (SRAH).pdf</a>
                    									</div>
                    									</div>
                    									<div class="form-group row">
                    									<label class="control-label col-md-3 col-sm-3 col-xs-12">Foto Tampak Bangunan Minimal 2 Sisi</label>
                    									<div class="col-md-3 col-sm-3 col-xs-12">
                    										<select disabled class="form-control" name="Jenis_IPTB">
                    										<option value="">Ada</option>
                    										<option value="dirut">Tidak Ada</option>
                    										</select>
                    									</div>
                    									<div class="col-md-3 col-sm-3 col-xs-12">
                    									<a href="#" download>Foto Tampak Bangunan Minimal 2 Sisi.jpg</a>
                    									</div>
                    									</div>

                    									<div class="form-group row">
                    									<label class="control-label col-md-3 col-sm-3 col-xs-12">Gambar Teknis</label>
                    									<div class="col-md-3 col-sm-3 col-xs-12">
                    									<a href="#" download>Gambar Teknis.jpg</a>
                    									</div>

                    									</div>

                    									<div class="form-group row">
                    									<label class="control-label col-md-3 col-sm-3 col-xs-12">Jumlah Sumur Yang Berfungsi</label>
                    									<div class="col-md-3 col-sm-3 col-xs-12">
                    										<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Jumlah SUmur Yang Berfungsi" readonly="">
                    									</div>

                    									<label class="control-label col-md-3 col-sm-3 col-xs-12">Volume Sumur Yang Berfungsi</label>
                    									<div class="col-md-3 col-sm-3 col-xs-12">
                    										<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Volume SUmur Yang Berfungsi"readonly="">
                    									</div>

                    									<label class="control-label col-md-3 col-sm-3 col-xs-12">Jumlah Kolam resapan Yang Berfungsi</label>
                    									<div class="col-md-3 col-sm-3 col-xs-12">
                    										<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Jumlah Kolam resapan Yang Berfungsi" readonly="">
                    									</div>

                    									<label class="control-label col-md-3 col-sm-3 col-xs-12">Volume Kolam resapan Yang Berfungsi</label>
                    									<div class="col-md-3 col-sm-3 col-xs-12">
                    										<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Volume Kolam resapan Yang Berfungsi" readonly="">
                    									</div>
                    									</div>

                    									<div class="form-group row">
                    									<label class="control-label col-md-3 col-sm-3 col-xs-12">Kesediaan Sanksi Untuk Ketidaksesuaaian</label>
                    									<div class="col-md-3 col-sm-3 col-xs-12">
                    										<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Kesediaan Sanksi Untuk Ketidaksesuaaian" readonly="">
                    									</div>
                                      <div class="form-group row">
                                        <br/>
                                        <label class="control-label col-md-1 col-sm-1 col-xs-12"></label>

                                        <button type="submit" name="submit" id="btnComment" class="btn btn-info" >Tambah Komentar</button>


                                      </div>

                    									</div>

                    					</div>
                            </div>
                            <div class="tab-pane fade in " id="tab6default">
                              <div id="data_perizinanlain">
                    						<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Sertifikat Laik Operasi</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download> Sertifikat Laik Operasi.pdf</a>
                    							</div>

                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                    							</div>
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="date" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                    							</div>
                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Pengesahan Pemakaian Instalasi Proteksi Kebakaran</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download> Pengesahan Pemakaian Instalasi Proteksi Kebakaran.pdf</a>
                    							</div>

                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                    							</div>
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="date" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                    							</div>
                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Pengesahan Pemakaian Pesawat Tenaga Produksi</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    							<a href="#" download> Pengesahan Pemakaian Pesawat Tenaga Produksi.pdf</a>
                    							</div>

                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                    							</div>
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="date" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                    							</div>
                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Pengesahan Pemakaian Bejana Token</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    							<a href="#" download> Pengesahan Pemakaian Pesawat Tenaga Produksi.pdf</a>
                    							</div>

                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                    							</div>
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="date" class="form-control" name="NAMA_IPTB" placeholder="Nomor"readonly>
                    							</div>
                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Pengesahan Pemakaian Instalasi Penyalur Petir</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    							<a href="#" download> Pengesahan Pemakaian Pesawat Tenaga Produksi.pdf</a>
                    							</div>

                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                    							</div>
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="date" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                    							</div>
                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Pengesahan Pemakaian Pesawat Angkat dan Angkut Lift</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    							<a href="#" download> Pengesahan Pemakaian Pesawat Angkat dan Angkut Lift.pdf</a>
                    							</div>

                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                    							</div>
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="date" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                    							</div>
                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Pengesahan Pemakaian Pesawat Angkat dan ANgkut Eskalator</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    							<a href="#" download> Pengesahan Pemakaian Pesawat Angkat dan ANgkut Eskalator.pdf</a>
                    							</div>

                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                    							</div>
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="date" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                    							</div>
                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Pengesahan Pemakaian Pesawat Angkat dan Angkut Gondola</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    							<a href="#" download>Pengesahan Pemakaian Pesawat Angkat dan Angkut Gondola.pdf</a>
                    							</div>

                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                    							</div>
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="date" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                    							</div>
                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Pengesahan Pemakaian Instalasi Listriki</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    							<a href="#" download> Pengesahan Pemakaian Instalasi Listriki.pdf</a>
                    							</div>

                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                    							</div>
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="date" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                    							</div>
                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Pengesahan Pemakaian Pesawat UAP</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    							<a href="#" download> Pengesahan Pemakaian Pesawat UAP.pdf</a>
                    							</div>

                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                    							</div>
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="date" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                    							</div>
                                  <div class="form-group row">
                                    <br/>
                                    <label class="control-label col-md-1 col-sm-1 col-xs-12"></label>

                                    <button type="submit" name="submit" id="btnComment" class="btn btn-info" >Tambah Komentar</button>


                                  </div>
                    							</div>
                    					</div>
                            </div>
                            <div class="tab-pane fade in " id="tab7default">
                              <div id="data_teknis">
                    						<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">As Built Drawing bangunan gedung yang telah ditanda tangani pemilik bangunan, kontraktor, kontraktor bangunan, dan pemegangn IPTB</label>


                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download> As Built Drawing bangunan gedung yang telah ditanda tangani pemilik bangunan, kontraktor, kontraktor bangunan, dan pemegangn IPTB.pdf</a>
                    							</div>


                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">As Built Drawing bangunan gedung bidang arsitektur, struktur, LAK, LAL, TDG, TUG dan SDP</label>


                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12"><h2>As Built Drawing bangunan gedung bidang arsitektur</h2></label>
                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Site Plan</label>

                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download> Site Plan.pdf</a>
                    							</div>

                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">SRAH & Detail SRAH</label>

                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download> SRAH & Detail SRAH.pdf</a>
                    							</div>


                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Potongan</label>

                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download> Potongan.pdf</a>
                    							</div>

                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Tampak</label>

                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download> Tampak.pdf</a>
                    							</div>

                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12"><h2>As Built Drawing bangunan gedung bidang Struktur</h2></label>
                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Denah Pondasi</label>

                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download> Denah Pondasi.pdf</a>
                    							</div>
                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Denah Balok, Kolom & Plat Lantai</label>

                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download> Denah Balok, Kolom & Plat Lantai.pdf</a>
                    							</div>
                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Detail Balok, Kolom & Plat Lantai</label>

                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download> Detail Balok, Kolom & Plat Lantai</a>
                    							</div>

                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Detail Sheer Wall & Core Wall</label>

                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download> Detail Sheer Wall & Core Wall</a>
                    							</div>

                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12"><h2>As Built Drawing bangunan gedung bidang ME</h2></label>
                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12"><h4>LAK</h4></label>
                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Diagram Skematik Sistem Distribusi Listrik</label>

                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download> Diagram Skematik Sistem Distribusi Listrik.pdf</a>
                    							</div>

                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Instalasi Penyalur Petir</label>

                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download> Instalasi Penyalur Petir.pdf</a>
                    							</div>

                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12"><h4>LAL</h4></label>
                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Diagram Skematik Sound Sistem</label>

                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download> Diagram Skematik Sound Sistem.pdf</a>
                    							</div>

                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Diagram Skematik File Alarm</label>

                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>Diagram Skematik File Alarm.pdf</a>
                    							</div>

                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Diagram Skematik Telepon</label>

                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>Diagram Skematik File Alarm.pdf</a>
                    							</div>

                    							</div>
                    							<div class="form-group row">
                    								<label class="control-label col-md-3 col-sm-3 col-xs-12"><h4>SDP</h4></label>
                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Diagram Skematik Pipa Pemadam kebakaran</label>

                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>Diagram Skematik Pipa Pemadam kebakaran.pdf</a>
                    							</div>

                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Diagram Skematik Air Bersih</label>

                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>Diagram Skematik Air Bersih.pdf</a>
                    							</div>

                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Diagram Skematik Air Kotor</label>

                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>Diagram Skematik Air Kotor.pdf</a>
                    							</div>

                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Diagram Skematik Air Hujan</label>

                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>Diagram Skematik Air Hujan.pdf</a>
                    							</div>


                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12"><h4>TDG</h4></label>
                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Diagram Skematik Lift</label>

                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>Diagram Skematik Lift.pdf</a>
                    							</div>

                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Diagram Skematik Gondola</label>

                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>Diagram Skematik Gondola.pdf</a>
                    							</div>

                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Diagram Skematik Eskalator</label>

                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>Diagram Skematik Eskalator.pdf</a>
                    							</div>
                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12"><h4>TUG</h4></label>
                    							</div>


                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Diagram Skematik Pressurized Fan</label>

                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>Diagram Skematik Pressurized Fan.pdf</a>
                    							</div>

                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Diagram Skematik Exhaust</label>

                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>Diagram Skematik Exhaust.pdf</a>
                    							</div>


                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Diagram Skematik Intahaust</label>

                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>Diagram Skematik Intahaust.pdf</a>
                    							</div>


                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Diagram Skematik Sistem Pendingin</label>

                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>Diagram Skematik ESistem Pendingin.pdf</a>
                    							</div>


                    							</div>





                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Surat Pernyataan Koordinator Direksi Pengawas atau Pengkaji Teknis</label>


                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>Surat Pernyataan Koordinator Direksi Pengawas atau Pengkaji Teknis.pdf</a>
                    							</div>

                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Surat Keterangan Selesai Membangun untuk SLF-1</label>


                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>Surat Keterangan Selesai Membangun untuk SLF-1.pdf</a>
                    							</div>


                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Laporan Direksi Pengawas untuk SLF-1 atau Laporan Kajian Teknis dari pengawas/pengkaji teknis yang mempunyai izin Pelaku Teknis Bangunan(IPTB)</label>


                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>Laporan Direksi Pengawas untuk SLF-1 atau Laporan Kajian Teknis dari pengawas/pengkaji teknis yang mempunyai izin Pelaku Teknis Bangunan(IPTB).pdf</a>
                    							</div>

                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Surat Pernyataan Kesanggupan Pembayaran Retribusi</label>


                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>Surat Pernyataan Kesanggupan Pembayaran Retribusi.pdf</a>
                    							</div>

                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Surat Pernyataan Pemilik Bangunan</label>


                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>Surat Pernyataan Kesanggupan Pembayaran Retribusi.pdf</a>
                    							</div>


                    							</div>
                    							<div class="form-group row" style="hidden">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12"><h4>Rekomendasi DCKTRP</h4></label>

                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>Rekomendasi DCKTRP.pdf</a>
                    							</div>


                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Intensitas Bangunan</label>


                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>Intensitas Bangunan.pdf</a>
                    							</div>

                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Berita Acara Bangunan</label>


                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>Intensitas Bangunan.pdf</a>
                    							</div>
                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12"><h4>Rekomendasi DAMKAR & Rekomendasi Keselamatan dan Kebakaran</h4></label>


                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>Rekomendasi DAMKAR & Rekomendasi Keselamatan dan Kebakaran.pdf</a>
                    							</div>

                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12"><h4>Rekomendasi Biro PKLH</h4></label>


                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>Rekomendasi Biro PKLH.pdf</a>
                    							</div>

                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Informasi Mengenai Daftar Kewajiban SPPT/IPPR/IPPT & Statusnya</label>


                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>Informasi Mengenai Daftar Kewajiban SPPT/IPPR/IPPT & Statusnya.pdf</a>
                    							</div>

                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Informasi BAPF</label>


                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>Informasi BAPF.pdf</a>
                    							</div>
                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Informasi BAST</label>


                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>Informasi BAST.pdf</a>
                    							</div>
                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Informasi Mengenai Kewajiban Sanksi S3PL</label>

                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>Informasi Mengenai Kewajiban Sanksi S3PL.pdf</a>
                    							</div>

                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Informasi Mengenai Kewajiban RSM, MJL, MDT, MHT dan Kewajiban lainnya</label>

                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>Informasi Mengenai Kewajiban RSM, MJL, MDT, MHT dan Kewajiban lainnya.pdf</a>
                    							</div>
                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12"><h4>Rekomendasi DLH & Tata Air</h4></label>

                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>File.pdf</a>
                    							</div>


                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Data Sistem Instalasi Pengolahan & Pembuangan Air</label>


                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>File.pdf</a>
                    							</div>
                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Data Pemakaian Air Bersih</label>


                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>File.pdf</a>
                    							</div>

                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Data Sumur Resapan</label>


                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>File.pdf</a>
                    							</div>

                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12"><h4>Rekomendasi BPRD</h4></label>


                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>File.pdf</a>
                    							</div>


                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Data SPPT PBB Bumi & Bangunan</label>


                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>File.pdf</a>
                    							</div>
                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12"><h4>Rekomendasi Biro Hukum</h4></label>


                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>File.pdf</a>
                    							</div>


                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Informasi Status Hukum Perusahaan</label>


                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>File.pdf</a>
                    							</div>
                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12"><h4>Rekomendasi NPAD Aset</h4></label>

                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>File.pdf</a>
                    							</div>

                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12"><h4>Dinas Tenaga Kerja</h4></label>


                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>File.pdf</a>
                    							</div>

                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Laporan Pemeriksaan Berkala</label>


                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>File.pdf</a>
                    							</div>

                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12"><h4>Dinas Bina Marga</h4></label>

                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>File.pdf</a>
                    							</div>


                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Informasi Inrit</label>


                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>File.pdf</a>
                    							</div>

                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Rekomendasi SKPD Lainnya</label>


                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>File.pdf</a>
                    							</div>

                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Rapim Gubernur/BKPRD</label>


                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>File.pdf</a>
                    							</div>

                    							</div>


                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Kajian Arsitektur</label>


                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>File.pdf</a>
                    							</div>
                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Kajian Struktur</label>


                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>File.pdf</a>
                    							</div>

                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Kajian ME</label>


                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>File.pdf</a>
                    							</div>

                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Berita Acara Pemeriksa Lapangan</label>


                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>File.pdf</a>
                    							</div>
                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Surat Pernyataan</label>


                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>File.pdf</a>
                    							</div>

                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Resume Analisa Kadis/Kabid</label>


                    								<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>File.pdf</a>
                    							</div>
                                  <div class="form-group row">
                                    <br/>
                                    <label class="control-label col-md-1 col-sm-1 col-xs-12"></label>

                                    <button type="submit" name="submit" id="btnComment" class="btn btn-info" >Tambah Komentar</button>


                                  </div>

                    							</div>
                    					</div>
                            </div>
                            <div class="tab-pane fade in " id="tab8default">
                              <div  id="data_imblama">
                    						<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor IMB</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<a href="#" download>File.pdf</a>
                    							</div>
                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="date" class="form-control" name="NAMA_IPTB" placeholder="Nama IPTB" readonly>
                    							</div>
                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Luas Seluruh Lantai</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Luas Seluruh Lantai" readonly>
                    							</div>
                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Jumlah Tower / Massa Bangunan</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Jumlah Tower / Massa Bangunan" readonly>
                    							</div>
                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Gambar Lampiran Bidang Arsitektur</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<select disabled class="form-control" name="Jenis_IPTB">
                    								<option value="">Ada</option>
                    								<option value="dirut">Tidak Ada</option>
                    								</select>
                    							</div>
                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Keterangan Rencana Kota (KRK)</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<select disabled class="form-control" name="Jenis_IPTB">
                    								<option value="">Ada</option>
                    								<option value="dirut">Tidak Ada</option>
                    								</select>
                    							</div>
                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor KRK</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                    							</div>
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="date" class="form-control" name="NAMA_IPTB" placeholder="Nama IPTB" readonly>
                    							</div>
                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12"> Rencana Tata Letak Bangunan (RTLB)</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<select disabled class="form-control" name="Jenis_IPTB">
                    								<option value="">Ada</option>
                    								<option value="dirut">Tidak Ada</option>
                    								</select>
                    							</div>
                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor RTLB</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                    							</div>
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="date" class="form-control" name="NAMA_IPTB" placeholder="Nama IPTB" readonly>
                    							</div>
                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">GPA</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<select disabled class="form-control" name="Jenis_IPTB">
                    								<option value="">Ada</option>
                    								<option value="dirut">Tidak Ada</option>
                    								</select>
                    							</div>
                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor GPA</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                    							</div>
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="date" class="form-control" name="NAMA_IPTB" placeholder="Nama IPTB" readonly>
                    							</div>
                                  <div class="form-group row">
                                    <br/>
                                    <label class="control-label col-md-1 col-sm-1 col-xs-12"></label>

                                    <button type="submit" name="submit" id="btnComment" class="btn btn-info" >Tambah Komentar</button>


                                  </div>
                    							</div>
                    					</div>
                            </div>
                            <div class="tab-pane fade in " id="tab9default">
                              <div id="slf_pernahterbit">
                    						<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    							<a href="#" download>File.pdf</a>
                    							</div>
                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="date" class="form-control" name="NAMA_IPTB" placeholder="Nama IPTB" readonly>
                    							</div>
                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Luas Seluruh Lantai</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Luas Seluruh Lantai" readonly>
                    							</div>
                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Jumlah Tower / Massa Bangunan</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="number" class="form-control" name="NAMA_IPTB" placeholder="Jumlah Tower / Massa Bangunan" readonly>
                    							</div>
                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Gambar Lampiran Bidang Arsitektur</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<select disabled class="form-control" name="Jenis_IPTB">
                    								<option value="">Ada</option>
                    								<option value="dirut">Tidak Ada</option>
                    								</select>
                    							</div>
                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Keterangan Rencana Kota (KRK)</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<select disabled class="form-control" name="Jenis_IPTB">
                    								<option value="">Ada</option>
                    								<option value="dirut">Tidak Ada</option>
                    								</select>
                    							</div>
                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                    							</div>
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="date" class="form-control" name="NAMA_IPTB" placeholder="Nama IPTB" readonly>
                    							</div>
                    							</div>

                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12"> Rencana Tata Letak Bangunan (RTLB)</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<select disabled class="form-control" name="Jenis_IPTB">
                    								<option value="">Ada</option>
                    								<option value="dirut">Tidak Ada</option>
                    								</select>
                    							</div>
                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                    							</div>
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="date" class="form-control" name="NAMA_IPTB" placeholder="Nama IPTB" readonly>
                    							</div>
                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">GPA</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<select disabled class="form-control" name="Jenis_IPTB">
                    								<option value="">Ada</option>
                    								<option value="dirut">Tidak Ada</option>
                    								</select>
                    							</div>
                    							</div>
                    							<div class="form-group row">
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Nomor</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="text" class="form-control" name="NAMA_IPTB" placeholder="Nomor" readonly>
                    							</div>
                    							<label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal</label>
                    							<div class="col-md-3 col-sm-3 col-xs-12">
                    								<input type="date" class="form-control" name="NAMA_IPTB" placeholder="Nama IPTB" readonly>
                    							</div>
                                  <div class="form-group row">
                                    <br/>
                                    <label class="control-label col-md-1 col-sm-1 col-xs-12"></label>

                                    <button type="submit" name="submit" id="btnComment" class="btn btn-info" >Tambah Komentar</button>


										</div>
                    				</div>
                    			</div>
                            </div>
                        </div>
                    </div>
                </div>

  




            <!-- <div class="form-group row">


            </div> -->






			<div class="ln_solid"></div>
			<div class="form-group row">
				<div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">

					<!-- <a href="#"><button type="submit" name="submit" class="btn btn-info" onclick="showDetail()">Cek Detail</button></a> -->
          <!--  -->

          <ul class="chec-radio">
          	<li class="pz">
          		<label class="radio-inline">
          			<input type="checkbox" id="pro-chx-residential" name="electricity_availability" class="pro-chx" value="yes" onchange="isChecked(this, 'btn1')">
          			<div class="clab">Verifikasi</div>
          		</label>
              <a href="<?php echo base_url('cek_berkas');?>"><button type="submit" name="submit" class="btn btn-success"
    						style="background-color:orange;" id="btn1" disabled>Lanjutkan</button></a>
          	</li>
          </ul>


				</div>
			</div>
		</div>
</div>
	
</div>

<!-- start form comment -->
<div class="bg-modal">
  <div class="modal-content">
    <div class="close">x</div>
    <div class="widget-area no-padding blank">
								<div class="status-upload">
									<form>
										<textarea placeholder="Cantumkan komentar" ></textarea>
										<ul>
											<li><a title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Audio"><i class="fa fa-music"></i></a></li>
											<li><a title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Video"><i class="fa fa-video-camera"></i></a></li>
											<li><a title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Sound Record"><i class="fa fa-microphone"></i></a></li>
											<li><a title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Picture"><i class="fa fa-picture-o"></i></a></li>
										</ul>
										<button type="submit" class="btn btn-success green"><i class="fa fa-save"></i> Tambahkan</button>
									</form>
								</div><!-- Status Upload  -->
							</div><!-- Widget Area -->
  </div>
</div>
<!-- end form comment -->

	<script src="<?php echo base_url('assets/bootstrap4/js/bootstrap.min.js'); ?>" type="text/javascript"></script>


	<script src="<?php echo base_url('assets/jquery/dist/jquery.min.js')?>"></script>
	<script src="<?php echo base_url('assets/build/js/custom.min.js')?>"></script>

<script type="text/javascript">
	function change() {
		var a = $('#alamat_ktp').val();
		var b = $("#prov").val();
		var c = $("#kecamatan").val();
		var d = $("#kelurahan").val();
		var e = $("#kota").val();

		$('#alamat_now').val(a);
		$("#prov2").val(b);
		$("#kecamatan2").val(c);
		$("#kelurahan2").val(d);
		$("#kota2").val(e);
	}

  document.getElementById('btnComment').addEventListener('click',
  function(){
    document.querySelector('.bg-modal').style.display = 'flex';
  });


  document.querySelector('.close').addEventListener('click',
  function(){
    document.querySelector('.bg-modal').style.display='none';
  });

	function isChecked(checkbox, btn1) {
    var button = document.getElementById(btn1);

    if (checkbox.checked === true) {
        button.disabled = "";
    } else {
        button.disabled = "disabled";
    }
}

	function showDetail() {
  		var x = document.getElementById("detail");
		var y = document.getElementById("form_detail")
  		if 	(x.style.display === "none" ) {
    		x.style.display = "block";
			y.style.display = "none";
  		} 	else {
    		x.style.display = "none";
			y.style.display = "block";
  		}
	}
	function showDataBangunan() {
  		var x = document.getElementById("data_bangunan");
  		if 	(x.style.display === "none") {
    		x.style.display = "block";
  		} 	else {
    		x.style.display = "none";
  		}
	}
	function showDataTanah() {
  		var x = document.getElementById("data_tanah");
  		if 	(x.style.display === "none") {
    		x.style.display = "block";
  		} 	else {
    		x.style.display = "none";
  		}
	}
	function ShowDataSIPPT() {
  		var x = document.getElementById("data_sippt");
  		if 	(x.style.display === "none") {
    		x.style.display = "block";
  		} 	else {
    		x.style.display = "none";
  		}
	}
	function ShowIptbArsitek() {
  		var x = document.getElementById("iptb_arsitek");
  		if 	(x.style.display === "none") {
    		x.style.display = "block";
  		} 	else {
    		x.style.display = "none";
  		}
	}
	function ShowIptbLal() {
  		var x = document.getElementById("iptb_lal");
  		if 	(x.style.display === "none") {
    		x.style.display = "block";
  		} 	else {
    		x.style.display = "none";
  		}
	}
	function ShowIptbLak() {
  		var x = document.getElementById("iptb_lak");
  		if 	(x.style.display === "none") {
    		x.style.display = "block";
  		} 	else {
    		x.style.display = "none";
  		}
	}
	function ShowIptbSdp() {
  		var x = document.getElementById("iptb_sdp");
  		if 	(x.style.display === "none") {
    		x.style.display = "block";
  		} 	else {
    		x.style.display = "none";
  		}
	}
	function ShowIptbTug() {
  		var x = document.getElementById("iptb_tug");
  		if 	(x.style.display === "none") {
    		x.style.display = "block";
  		} 	else {
    		x.style.display = "none";
  		}
	}
	function ShowIptbTdg() {
  		var x = document.getElementById("iptb_tdg");
  		if 	(x.style.display === "none") {
    		x.style.display = "block";
  		} 	else {
    		x.style.display = "none";
  		}
	}
	function ShowDataSumur() {
  		var x = document.getElementById("data_sumur");
  		if 	(x.style.display === "none") {
    		x.style.display = "block";
  		} 	else {
    		x.style.display = "none";
  		}
	}
	function ShowDataIzinlain() {
  		var x = document.getElementById("data_perizinanlain");
  		if 	(x.style.display === "none") {
    		x.style.display = "block";
  		} 	else {
    		x.style.display = "none";
  		}
	}
	function ShowDataTeknis() {
  		var x = document.getElementById("data_teknis");
  		if 	(x.style.display === "none") {
    		x.style.display = "block";
  		} 	else {
    		x.style.display = "none";
  		}
	}
	function ShowDataIMBlama() {
  		var x = document.getElementById("data_imblama");
  		if 	(x.style.display === "none") {
    		x.style.display = "block";
  		} 	else {
    		x.style.display = "none";
  		}
	}
	function ShowSlfPernahTerbit() {
  		var x = document.getElementById("slf_pernahterbit");
  		if 	(x.style.display === "none") {
    		x.style.display = "block";
  		} 	else {
    		x.style.display = "none";
  		}
	}


</script>
