<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>

<div class="col-md-12 col-xs-12">
  <div class="x_panel">

    <div class="x_title">
      <h2>Form Permohonan</h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
              class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>

    <div class="x_content">
      <br />
      <!-- <form class="form-horizontal form-label-left"> -->
      <form action="<?= base_url('data_bangunan/add')?>" method="post">
        <!-- <input type="text" class="form-control" name="ID_PENGELOLA" placeholder="Nomor KTP" value="<?php echo $kodeunik; ?>" > -->
        <div class="col-md-4 col-sm-12 col-xs-12">
          <div class="form-group col-md-12 col-sm-12 col-xs-12">
      
            <input type="hidden" class="form-control" name="id_bangunan" placeholder="ID Kawasan" readonly=""
              value="<?php echo $id_bangunan;?>">
          </div>

          <div class="form-group col-md-12 col-sm-12 col-xs-12">
            <label class="control-label ">Nama Kawasan</label>
            <input type="text" class="form-control" name="nama_kawasan" placeholder="Nama Kawasan" readonly=""
              value="<?php echo $NmBangunan[0]['NM_BANGUNAN']?>">
          </div>

          <div class="form-group col-md-12 col-sm-12 col-xs-12">
            <label class="control-label ">Jenis Permohonan</label>
            <select class="form-control" name="jenis_permohonan" id="jenis_permohonan" onchange="showSifat(this)">
              <option value="slfbaru">Pengajuan Permohonan SLF Baru</option>
              <option value="perpanjangslf">Pengajuan Perpanjangan SLF</option>
            </select>
          </div>

          <div class="form-group col-md-12 col-sm-12 col-xs-12">
            <label class="control-label">Pilih Tower</label>
            <select class="form-control" name="nama_tower[]">
              <?php foreach ($NmTower as $tower):?>
              <option value="<?php echo $tower['ID_TEKNIS_TOWER'];?>"><?php echo $tower['NAMA_TOWER'];?></option>
              <?php endforeach;?>
            </select>
          </div>



          <div class="form-group col-md-12 col-sm-12 col-xs-12">
            <div class="input-group-btn" style="margin-top:5px">

              <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i>
                Tambah</button>
            </div>
          </div>
        </div>

        <div class="panel-body">
          <div class="input-group control-group after-add-more">
          </div>


          <!-- Copy Fields -->
          <div class="copy hide">
            <div class="control-group input-group" style="margin-top:10px">
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Pilih Tower</label>
                <div class="col-md-3 col-sm-3 col-xs-12">
                  <select class="form-control" name="nama_tower[]">
                    <?php foreach ($NmTower as $tower):?>
                    <option value="<?php echo $tower['ID_TEKNIS_TOWER'];?>"><?php echo $tower['NAMA_TOWER'];?></option>
                    <?php endforeach;?>
                  </select>
                </div>
              </div>
              <div class="input-group-btn">
                <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i>
                  Hapus</button>
              </div>
            </div>
          </div>


          <!--
        <div class="ln_solid"></div> -->
          <div class="form-group">
            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">


              <button class="btn btn-info remove" type="submit" name="submit">Proses</button>


            </div>
          </div>
        </div>
        </form>
    </div>
  </div>
  
  <script type="text/javascript">
    $(document).ready(function () {
      $(".add-more").click(function () {
        var html = $(".copy").html();
        $(".after-add-more").after(html);
      });
      $("body").on("click", ".remove", function () {
        $(this).parents(".control-group").remove();
      });
    });
  </script>