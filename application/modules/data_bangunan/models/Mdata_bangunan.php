<?php

class Mdata_bangunan extends CI_Model{
    function savePengajuan($kode_pengajuan){
        $statuspengajuan = "diproses";
        $data = array(
            'ID_BANGUNAN'        => $this->input->post('id_bangunan', TRUE),
            "STATUS_PENGAJUAN"   => $statuspengajuan,
            'KODE_PENGAJUAN'     => $kode_pengajuan   
        );
        // print_r($data);
        // exit;
        
        $this->db->insert('SYSTEM.PENGAJUAN', $data);
    }

    function savePengajuanDetail($i, $kode_pengajuan){
        $data = array(
            'ID_BANGUNAN'       => $this->input->post('id_bangunan', TRUE),
            'ID_TEKNIS_TOWER'   => $i,
            'KODE_PENGAJUAN'    => $kode_pengajuan
        );
        // print_r($data);
        // exit;

        $this->db->insert('SYSTEM.PENGAJUAN_DETAILasds', $data);
    }

    function getIdPengajuan($kode_pengajuan){
        $this->db->select('ID_PENGAJUAN');
        $this->db->where('KODE_PENGAJUAN =', $kode_pengajuan);
        $query = $this->db->get('SYSTEM.PENGAJUAN');
        if ($query->num_rows())
        {
            return $query->result_array();
        }
        else
        {
            return FALSE;
        }
    }

    
    function getDataBangunan($id_bangunan){
        $this->db->select('NM_BANGUNAN,');
        $this->db->where('ID_BANGUNAN =', $id_bangunan);
        $query = $this->db->get('SYSTEM.BANGUNAN');
        if ($query->num_rows())
        {
            return $query->result_array();
        }
        else
        {
            return FALSE;
        }
    }

    function getDataTower($id_bangunan){
        $this->db->select('NAMA_TOWER, ID_TEKNIS_TOWER');
        $this->db->where('ID_BANGUNAN =', $id_bangunan);
        $query = $this->db->get('SYSTEM.TEKNIS_BANGUNAN_TOWER');
        if ($query->num_rows())
        {
            return $query->result_array();
        }
        else
        {
            return FALSE;
        }
    }

}
