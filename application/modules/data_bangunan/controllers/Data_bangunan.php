<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
        
class Data_bangunan extends MY_Controller {

function __construct()
 	{
 		parent::__construct();
 		$this->load->model('Mdata_bangunan');
 		$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
 		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
 		$this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
 		$this->output->set_header('Pragma: no-cache');
 	}

	public function index()
	{

		$data['group_menu'] = 'hr';
		$data['content']    = 'data_bangunan';

		
		$this->load->view('base/index', $data);
    }

    function add(){
        if (isset($_POST['submit'])) {
            $data['id_bangunan'] = $this->input->POST('id_bangunan');
            
            $kode_pengajuan = "P".rand(1,100).date("ymd");
            $this->Mdata_bangunan->savePengajuan($kode_pengajuan);
            $idtower= $this->input->post('nama_tower[]', TRUE);
            // $x=$this->input->post('group-b[]');
            // $x=array_unique($x);
            // print_r($x);
            // exit;
            $idtower=array_unique($idtower);
            foreach($idtower as $i){
                $this->Mdata_bangunan->savePengajuanDetail($i, $kode_pengajuan);
            }
            $idpengajuan = $this->Mdata_bangunan->getIdPengajuan($kode_pengajuan);
            
            redirect('detail_bangunan?id='.$idpengajuan[0]['ID_PENGAJUAN'].'&idbngn='.$data['id_bangunan']);
        }else{
            $data['id_bangunan'] = $this->uri->segment(3);
            $data['NmBangunan'] = $this->Mdata_bangunan->getDataBangunan($data['id_bangunan']);
            $data['NmTower'] = $this->Mdata_bangunan->getDataTower($data['id_bangunan']);
            $data['group_menu'] = 'hr';
		    $data['content']    = 'data_bangunan';

		
		    $this->load->view('base/index', $data);
        }
    }

    function baru(){
        echo 'oke';
        print_r($this->input->POST());
        exit;
    }
    

	
}