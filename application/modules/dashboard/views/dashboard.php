
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

<div class="container">
  <div class="card mb-3">
    <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item active" aria-current="page">Halaman Utama</li>
    </ol>
    </nav>
<!--
        <div class="card-body">
          <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        </div> -->
        <div class="card-body">
          <div class="row">
            <div class="col-md-3">
              <div class="list-group">
                <a href="http://www.jquery2dotnet.com" class="list-group-item visitor">
                    <h3 class="pull-right">
                        <i class="fa fa-map-marker"></i>
                    </h3>
                    <h4 class="list-group-item-heading count">
                        <?php echo $JmlKawasan[0]['JMLKAWASAN'];?></h4>
                    <p class="list-group-item-text">
                        Kawasan/Gedung</p>
                </a>
            </div>
          </div>
          <div class="col-md-3">
            <div class="list-group">
              <a href="http://www.jquery2dotnet.com" class="list-group-item visitor">
                  <h3 class="pull-right">
                      <i class="fa fa-building"></i>
                  </h3>
                  <h4 class="list-group-item-heading count">
                      <?php echo $JmlTower[0]['JMLTOWER']; ?></h4>
                  <p class="list-group-item-text">
                      Tower</p>
              </a>
          </div>
        </div>
        <div class="col-md-3">
          <div class="list-group">
            <a href="http://www.jquery2dotnet.com" class="list-group-item visitor">
                <h3 class="pull-right">
                    <i class="fa fa-check-circle"></i>
                </h3>
                <h4 class="list-group-item-heading count">
                     <?php echo $JmlSLFTower[0]['JMLSLFTOWER']; ?></h4>
                <p class="list-group-item-text">
                    Tower Sudah SLF</p>
            </a>
        </div>
      </div>
      <div class="col-md-3">
        <div class="list-group">
          <a href="http://www.jquery2dotnet.com" class="list-group-item visitor">
              <h3 class="pull-right">
                  <i class="fa fa-check-circle"></i>
              </h3>
              <h4 class="list-group-item-heading count">
                  2</h4>
              <p class="list-group-item-text">
                  Tower belum SLF</p>
          </a>
      </div>
    </div>
        </div>
          <div class="row">
            <div class = "col-md-6">
              <div id="container2" ></div>
            </div>
            <div class = "col-md-6">
              <div id="container3" ></div>
            </div>
          </div>
        </div>
        
    </div>
  </div>
  <br>
  <br>
   <div class="list-group-item visitor" style="width: 50%;">
        <h1> Iklan</h1>
        <p> Iklan Iklan Iklan Iklan Iklan Iklan Iklan Iklan Iklan Iklan Iklan Iklan Iklan Iklan  Iklan Iklan</p>
        <h1> Iklan</h1>
        <p> Iklan Iklan Iklan Iklan Iklan Iklan Iklan Iklan Iklan Iklan Iklan Iklan Iklan Iklan  Iklan Iklan</p>
        <h1> Iklan</h1>
        <p> Iklan Iklan Iklan Iklan Iklan Iklan Iklan Iklan Iklan Iklan Iklan Iklan Iklan Iklan  Iklan Iklan</p>
        <h1> Iklan</h1>
        <p> Iklan Iklan Iklan Iklan Iklan Iklan Iklan Iklan Iklan Iklan Iklan Iklan Iklan Iklan  Iklan Iklan</p>
        <h1> Iklan</h1>
        <p> Iklan Iklan Iklan Iklan Iklan Iklan Iklan Iklan Iklan Iklan Iklan Iklan Iklan Iklan  Iklan Iklan</p>
   </div>
   <br>
</div>



<script>
// Highcharts.chart('container', {
//     chart: {
//         type: 'column'
//     },
//     title: {
//         text: 'Ambang Layak SLF '
//     },
//     subtitle: {
//         text: ''
//     },
//     xAxis: {
//         categories: [
//             'Tower 1 A',
//             'Tower 1 B',
//             'Tower 1 C'
//         ],
//         crosshair: true
//     },
//     yAxis: {
//         min: 0,
//         title: {
//             text: 'Kelengkapan'
//         }
//     },
//     credits: {
//        enabled: false
//      },
//     tooltip: {
//         headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
//         pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
//             '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
//         footerFormat: '</table>',
//         shared: true,
//         useHTML: true
//     },
//     plotOptions: {
//         column: {
//             pointPadding: 0.2,
//             borderWidth: 0
//         }
//     },
//     series: [{
//         name: 'Gambar teknis',
//         data: [4, 5, 5]
//
//     }, {
//         name: 'Rekomendasi SKPD terkait',
//         data: [4, 4, 3]
//
//     }, {
//         name: 'Data perizinan terkait',
//         data: [5, 5, 5]
//
//     }, {
//         name: 'IPTB',
//         data: [5, 6, 5]
//
//     },
//     {
//         name: 'Kesesuaian RTDR / PZ',
//         data: [5, 5, 6]
//
//     }, {
//         name: 'Data SIPTT',
//         data: [4, 3, 4]
//
//     },{
//         name: 'Data Sertifikat dan PBB',
//         data: [3, 4, 4]
//
//     }, {
//         name: 'Data umum bangunan/tower',
//         data: [2, 3, 3]
//
//     }]
// });

Highcharts.chart('container2', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    credits: {
       enabled: false
     },
    title: {
        text: 'Status Tower'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
    series: [{
        name: 'Proses',
        colorByPoint: true,
        data: [{
            name: 'Belum SLF',
            y: 61.41,
            sliced: true,
            selected: true
        }, {
            name: 'Sudah SLF',
            y: 11.84
        }]
    }]
});

Highcharts.chart('container3', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Jumlah permohonan'
    },
    credits: {
       enabled: false
     },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
    series: [{
        name: 'Permohonan',
        colorByPoint: true,
        data: [{
            name: 'SLF Baru',
            y: 61.41,
            sliced: true,
            selected: true
        }, {
            name: 'SLF Perpanjangan',
            y: 11.84
        }]
    }]

  });
</script>
