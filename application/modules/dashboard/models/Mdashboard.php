<?php
class Mdashboard extends MY_Model {
    function getJmlTower(){
        $this->db->select('COUNT(TEKNIS_BANGUNAN_TOWER.ID_TEKNIS_TOWER) as JmlTower');
        $this->db->from('SYSTEM.TEKNIS_BANGUNAN_TOWER');
        $this->db->join('SYSTEM.BANGUNAN','TEKNIS_BANGUNAN_TOWER.ID_BANGUNAN = BANGUNAN.ID_BANGUNAN','inner');
        $this->db->where('BANGUNAN.ID_PEMILIK = 2');
        $query=$this->db->get();
        if ($query->num_rows())
        {
            return $query->result_array();
        }
        else
        {
            return FALSE;
        }
    }

    function getJmlKawasan(){
        $this->db->select('COUNT(ID_BANGUNAN) as JmlKawasan');
        $this->db->where('ID_PEMILIK = 1');
        $query = $this->db->get('SYSTEM.BANGUNAN');
        if ($query->num_rows())
        {
            return $query->result_array();
        }
        else
        {
            return FALSE;
        }
    }

    function getDataPermohonan(){
        $this->db->select('NM_BANGUNAN, LOKASI_BANGUNAN');
        $query = $this->db->get('SYSTEM.BANGUNAN');
        if ($query->num_rows())
        {
            return $query->result_array();
        }
        else
        {
            return FALSE;
        }
    }

    function getTowersudahSLF(){
        $this->db->select('COUNT(SLF_TOWER.ID_SLF_TOWER) as JmlSLFTower');
        $this->db->from('SYSTEM.SLF_TOWER');
        $this->db->join('SYSTEM.SLF','SLF.ID_SLF = SLF_TOWER.ID_SLF','inner');
        $this->db->where('SLF.ID_PEMILIK = 1');
        $query=$this->db->get();
        if ($query->num_rows())
        {
            return $query->result_array();
        }
        else
        {
            return FALSE;
        }
        
    }
}

?>
