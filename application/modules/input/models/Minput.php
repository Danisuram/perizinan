<?php
class Minput extends MY_Model {
    function getDataPermohonan(){

        $this->db->select('COUNT(TEKNIS_BANGUNAN_TOWER.ID_TEKNIS_TOWER) AS JMLTOWER, BANGUNAN.NM_BANGUNAN, BANGUNAN.LOKASI_BANGUNAN, BANGUNAN.ID_BANGUNAN');
        $this->db->from('SYSTEM.TEKNIS_BANGUNAN_TOWER');
        $this->db->join('SYSTEM.BANGUNAN',' ON TEKNIS_BANGUNAN_TOWER.ID_BANGUNAN = BANGUNAN.ID_BANGUNAN','right outer');
        $this->db->where('ID_PEMILIK', $this->session->userdata('id'));
        $this->db->group_by('BANGUNAN.NM_BANGUNAN, BANGUNAN.LOKASI_BANGUNAN, BANGUNAN.ID_BANGUNAN');
        $query = $this->db->get();
        if ($query->num_rows())
        {
            return $query->result_array();
        }
        else
        {
            return FALSE;
        }
    }
}