<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div class="kt-subheader   kt-grid__item" id="kt_subheader">

</div>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">


  <div class="card mb-3">
        <!-- <div class="card-header">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
            </ol>
          </nav>
        </div> -->
        <div class="card-body">
                             <?php

                             //$this->load->library('Gate_keeper');

							// print_r($this->session->userdata('unm')); ?>
          <div class="table-responsive">
          <table id="m_artikel" class="table table-striped table-bordered">
          <thead>
              <tr>
                  <th>Lokasi</th>
                  <th>Nama Gedung / Kawasan</th>
                  <th>Jumlah Tower</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($DataPermohonan as $data):?>
                  <tr>
                    <td><?php echo $data['LOKASI_BANGUNAN']?></td>
                    <td><?php echo $data['NM_BANGUNAN']?> </td>
                    <td><?php echo $data['JMLTOWER']?></td>
                    <td><a href="<?= base_url('data_bangunan/add/'.$data['ID_BANGUNAN']);?>"><button type="submit" name="submit" class="btn btn-success" style="background-color:green;"><i class="fas fa-file"></i></button>
                      <!-- <button type="submit" name="submit" class="btn btn-info"><i class="icon-info-sign"></i> </button> --></td>
                  </tr>
                <?php endforeach;?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
</div>
