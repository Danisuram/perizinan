<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	 function __construct()
	 {
		 parent::__construct();

		 $this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
		 $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
		 $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
		 $this->output->set_header('Pragma: no-cache');

	 }

	public function index()


	{
		$array = array('log_userid'=>'1',
													'log_date'=>'1',
													'log_visit'=>'1',
													'log_ip'=>'1');
		$this->session->set_userdata($array);
		$this->load->view('login');

	}

	function login_check() {

			 $usr_login  = $this->input->post('username');
			 $password   = $this->input->post('password');
			 $password   = md5($password);

			 if (!empty($usr_login) and !empty($password)) {
	 
					 $this->gate_keeper->login_routine($usr_login, $password);
			 }else{
					 redirect('login');
			 }
	 }
	 
	function logout(){
	$this->gate_keeper->logout_routine();
	//redirect('login');
	}
	


}
