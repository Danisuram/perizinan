<?php 

defined('BASEPATH') OR exit('No direct script access allowed');
        
class Formulir extends MY_Controller {

function __construct()
 	{
 		parent::__construct();
 		$this->load->model('Mformulir');
 		$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
 		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
 		$this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
 		$this->output->set_header('Pragma: no-cache');
 	}

	public function index()
	{

		$data['group_menu'] = 'hr';
		$data['content']    = 'formulir';

		
		$this->load->view('base/index', $data);
	}
    
    function add(){
        if (isset($_POST['submit'])) {

            // $this->model_iptb->save();
            redirect('formulir');
        }else{
            // $data['kodeunik'] = $this->model_pengelola_bangunan->buat_kode();
            $data['idpengajuan'] = $this->uri->segment(3);
            // print_r($data);
            // exit;
            $data['group_menu'] = 'hr';
		    $data['content']    = 'formulir';
		    $this->load->view('base/index', $data);
        }
    }

    function upload_permohonanSLF(){
        $config['upload_path']          = './assets/document/permohonanSLF';
        $config['allowed_types']        = 'doc|docx|pdf';
        $config['max_size']             = 50000;
        $config['max_width']            = 1024;
        $config['max_height']           = 768;

        $this->load->library('upload', $config);



        if ( ! $this->upload->do_upload('file_permohonanSLF'))
        {
            echo '<script language="javascript">';
            echo 'alert("File Gagal di Upload")';
            echo '</script>';  
            redirect('formulir/add/'.$idpermohonan); 
        }
        else
        {
                echo ' <script language="javascript"> alert("File Berhasil di Upload")</script>';
                

                $url = $config['upload_path']."/".$this->upload->data('file_name');
                $idpengajuan = $this->input->post('idpengajuan');
         
                $this->Mformulir->saveFilePermohonan($url, $idpengajuan);

                redirect('formulir/add/'.$idpermohonan);
                
        }
    }

    function upload_kebenaranDokumen(){
        $config['upload_path']          = './assets/document/kebenaranDokumen';
        $config['allowed_types']        = 'doc|docx|pdf';
        $config['max_size']             = 50000;
        $config['max_width']            = 1024;
        $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('file_kebenaranDokumen'))
        {
            echo '<script language="javascript">';
            echo 'alert("File Gagal Di Upload")';
            echo '</script>';  
            redirect('formulir/add/'.$idpermohonan); 
        }
        else
        {
            echo ' <script language="javascript"> alert("File Berhasil di Upload")</script>';
            $url = $config['upload_path']."/".$this->upload->data('file_name');
            $idpengajuan = $this->input->post('idpengajuan');
            
            $this->Mformulir->saveFileKebenaran($url, $idpengajuan);
            redirect('formulir/add/'.$idpermohonan);
        }
    }
}