<?php

class Mformulir extends CI_Model{

    function saveFilePermohonan($url, $idpengajuan){
        $this->db->set('PATH_SURATPERNYATAAN', $url);
        $this->db->where('ID_PENGAJUAN =', $idpengajuan);
        $this->db->update('SYSTEM.PENGAJUAN');
    }

    function saveFileKebenaran($url, $idpengajuan){
        $this->db->set('PATH_PERNYATAANKEBENARAN', $url);
        $this->db->where('ID_PENGAJUAN =', $idpengajuan);
        $this->db->update('SYSTEM.PENGAJUAN');
    }
}