<div class="col-md-12 col-xs-12">
  <div class="x_panel">

    <div class="x_title">
      <h2>Lengkapi Berkas<small></small></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>

    <div class="x_content">
      <br />

     

     
        <table id="m_artikel" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Nama Surat</th>
                <th>Template Surat</th>
                <th>Upload Surat</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <tr>
              
              <?php echo form_open_multipart('formulir/upload_permohonanSLF');?>
              <input type="hidden" name="idpengajuan" value='<?php echo $idpengajuan?>'>
                <td><label class="">Surat Pernyataan Pemohon Untuk Kegiatan SLF</label></td>
                <td><a href="<?php echo base_url('assets/document/permohonanSLF/template/1. Surat Pernyataan Pemohon untuk Kegiatan Permohonan SLF.docx');?>" download class="btn btn-success" style="background-color:orange;">Unduh</a></td>
                <td><input type="file" name="file_permohonanSLF"></td>
                <input type="hidden" name = "idpengajuan"  value="<?php echo $idpengajuan; ?>">
                <td><input class="btn btn-success" type="submit" value="upload" /></td>
                </form>
               </tr>
               <tr>
              
                <?php echo form_open_multipart('formulir/upload_kebenaranDokumen');?>
                <input type="hidden" name="idpengajuan" value='<?php echo $idpengajuan?>'>
                <td><label class="">Permohonan Serta Pernyataan Kebenaran & Keabsahan Dokumen atas Sertifikat Layak Fungsi</label></td>
                <td><a href="<?php echo base_url('assets/document/kebenaranDokumen/template/2. Surat Permohonan serta Kebenaran & Keabsahan Dokumen atas Sertifikat Laik Fungsi.docx');?>" download class="btn btn-success" style="background-color:orange;">Unduh</a></td>
                <td><input type="file" name="file_kebenaranDokumen"></td>
                <td><input class="btn btn-success" type="submit" value="upload" /></td>
                </form>
              </tr>
            </tbody>
        </table>
        <!-- <input type="file" name="userfileu" size="20" />
        <input class="btn btn-success" type="submit" value="upload" /> -->
        
        <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
          <a href="<?php echo base_url('statusdiproses');?>"><button type="submit" name="submit" class="btn btn-success" style="background-color:orange;">Serahkan</button>
          </div>
        </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  function change() {
    var a = $('#alamat_ktp').val();var b = $("#prov").val();var c = $("#kecamatan").val();var d = $("#kelurahan").val();var e = $("#kota").val();

    $('#alamat_now').val(a);$("#prov2").val(b);$("#kecamatan2").val(c);$("#kelurahan2").val(d);$("#kota2").val(e);
  }

</script>
