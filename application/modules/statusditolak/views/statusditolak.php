<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

</head>

<div class="kt-subheader   kt-grid__item" id="kt_subheader">

</div>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">



  <div class="card mb-3">
        <div class="card-header">
          <h3>Status Ditolak</h3>
        </div>
        <div class="card-body">

          <div class="table-responsive">
          <table id="m_artikel" class="table table-striped table-bordered">
              <thead>
              <tr>
                  <th>Nama Gedung / Kawasan</th>
                  <th>Nama Tower</th>
                  <th>Jenis Permohonan</th>
                  <th>Tanggal Pengajuan</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody>

                <tr>
                  <td>Green Garden Tower</td>
                  <td>Tower 1A</td>
                  <td>Pengajuan Pembuatan SLF Baru</td>
                  <td> 19/09/2019</td>
                  <td>Ditolak</td>

                </tr>


              </tbody>
            </table>
          </div>
        </div>
      </div>

