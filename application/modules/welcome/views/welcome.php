<div class="kt-subheader   kt-grid__item" id="kt_subheader">

</div>

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>



<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">



  <div class="card mb-3">
    <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item active" aria-current="page">Halaman Utama</li>
    </ol>
    </nav>

        <div class="card-body">
          <div class="row">
            <div class = "col-md-6">
              <div id="container1" ></div>
            </div>
            <div class = "col-md-6">
              <div id="container2" ></div>
            </div>
          </div>
        </div>
        <br/>
        <div class="card-body">
          <div class="row">
            <div class = "col-md-6">
              <div id="container3" ></div>
            </div>
            <div class = "col-md-6">
              <div id="container4" ></div>
            </div>
          </div>
        </div>
        <br/>
        <div class="card-body">
          <div class="row">
            <div class = "col-md-6">
              <div id="container5" ></div>
            </div>
            <div class = "col-md-6">
              <div id="container6" ></div>
            </div>
          </div>
        </div>
        <br/>
        <div class="card-body">
          <div class="row">
            <div class = "col-md-6">
              <div id="container7" ></div>
            </div>
            <div class = "col-md-6">
              <div id="container8" ></div>
            </div>
          </div>
        </div>
  </div>


<script>


Highcharts.chart('container1', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    credits: {
       enabled: false
     },
    title: {
        text: 'Jumlah'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
    series: [{
        name: 'Proses',
        colorByPoint: true,
        data: [{
            name: 'Proses CFO',
            y: 61.41,
            sliced: true,
            selected: true
        }, {
            name: 'Ditolak',
            y: 11.84
        }, {
            name: 'Terbit resume penilaian teknis dan survey',
            y: 10.85
        }, {
            name: 'Persetujuan teknis',
            y: 4.67
        }, {
            name: 'Disetujui CFO',
            y: 4.18
        }, {
            name: 'Proses penilaian teknis dan survey',
            y: 1.64
        }, {
            name: 'Perhitungan retribusi/NPR',
            y: 1.6
        }, {
            name: 'Penetapan SLF',
            y: 1.2
        }]
    }]
});

Highcharts.chart('container2', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Jumlah permohonan'
    },
    credits: {
       enabled: false
     },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
    series: [{
        name: 'Permohonan',
        colorByPoint: true,
        data: [{
            name: 'SLF Baru',
            y: 61.41,
            sliced: true,
            selected: true
        }, {
            name: 'SLF Perpanjangan',
            y: 11.84
        }]
    }]

  });

  Highcharts.chart('container3', {
      chart: {
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          type: 'pie'
      },
      credits: {
         enabled: false
       },
      title: {
          text: 'Jenis Bangunan Mengajukan SLF'
      },
      tooltip: {
          pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
          pie: {
              allowPointSelect: true,
              cursor: 'pointer',
              dataLabels: {
                  enabled: true,
                  format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                  style: {
                      color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                  }
              }
          }
      },
      series: [{
          name: 'Proses',
          colorByPoint: true,
          data: [{
              name: 'Apartemen',
              y: 61.41,
              sliced: true,
              selected: true
          }, {
              name: 'Rumah Susun',
              y: 11.84
          }, {
              name: 'Perkantoran',
              y: 10.85
          }, {
              name: 'Pusat Perbelanjaan',
              y: 4.67
          }, {
              name: 'Hotel',
              y: 4.18
          }]
      }]
  });

  Highcharts.chart('container4', {
      chart: {
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          type: 'pie'
      },
      title: {
          text: 'Status SLF Jakarta Barat'
      },
      credits: {
         enabled: false
       },
      tooltip: {
          pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
          pie: {
              allowPointSelect: true,
              cursor: 'pointer',
              dataLabels: {
                  enabled: true,
                  format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                  style: {
                      color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                  }
              }
          }
      },
      series: [{
          name: 'Permohonan',
          colorByPoint: true,
          data: [{
              name: 'Jakarta Barat (Sudah SLF)',
              y: 61,
              sliced: true,
              selected: true
          }, {
              name: 'Jakarta Barat (Belum SLF)',
              y: 10
          }]
      }]

    });

    Highcharts.chart('container5', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Status SLF Jakarta Selatan'
        },
        credits: {
           enabled: false
         },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Permohonan',
            colorByPoint: true,
            data: [{
                name: 'Jakarta Selatan (Sudah SLF)',
                y: 180,
                sliced: true,
                selected: true
            }, {
                name: 'Jakarta Selatan (Belum SLF)',
                y: 22
            }]
        }]

      });

      Highcharts.chart('container6', {
          chart: {
              plotBackgroundColor: null,
              plotBorderWidth: null,
              plotShadow: false,
              type: 'pie'
          },
          title: {
              text: 'Status SLF Jakarta Timur'
          },
          credits: {
             enabled: false
           },
          tooltip: {
              pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
          },
          plotOptions: {
              pie: {
                  allowPointSelect: true,
                  cursor: 'pointer',
                  dataLabels: {
                      enabled: true,
                      format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                      style: {
                          color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                      }
                  }
              }
          },
          series: [{
              name: 'Permohonan',
              colorByPoint: true,
              data: [{
                  name: 'Jakarta Timur (Sudah SLF)',
                  y: 140,
                  sliced: true,
                  selected: true
              }, {
                  name: 'Jakarta Timur (Belum SLF)',
                  y: 12
              }]
          }]

        });

        Highcharts.chart('container7', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Status SLF Jakarta Pusat'
            },
            credits: {
               enabled: false
             },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                name: 'Permohonan',
                colorByPoint: true,
                data: [{
                    name: 'Jakarta Pusat (Sudah SLF)',
                    y: 150,
                    sliced: true,
                    selected: true
                }, {
                    name: 'Jakarta Pusat (Belum SLF)',
                    y: 11
                }]
            }]

          });

          Highcharts.chart('container8', {
              chart: {
                  plotBackgroundColor: null,
                  plotBorderWidth: null,
                  plotShadow: false,
                  type: 'pie'
              },
              title: {
                  text: 'Status SLF Jakarta Utara'
              },
              credits: {
                 enabled: false
               },
              tooltip: {
                  pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
              },
              plotOptions: {
                  pie: {
                      allowPointSelect: true,
                      cursor: 'pointer',
                      dataLabels: {
                          enabled: true,
                          format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                          style: {
                              color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                          }
                      }
                  }
              },
              series: [{
                  name: 'Permohonan',
                  colorByPoint: true,
                  data: [{
                      name: 'Jakarta Utara (Sudah SLF)',
                      y: 111,
                      sliced: true,
                      selected: true
                  }, {
                      name: 'Jakarta Utara (Belum SLF)',
                      y: 21
                  }]
              }]

            });
</script>


</div>
