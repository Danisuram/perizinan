<!DOCTYPE html>

<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4 & Angular 7
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en">

	<!-- begin::Head -->
	<head>

		<!--begin::Base Path (base relative path for assets of this page) -->
		<base href="../">

		<!--end::Base Path -->
		<meta charset="utf-8" />
		<title>PTSP Perizinan</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!--begin::Fonts -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
				google: {
					"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
				},
				active: function() {
					sessionStorage.fonts = true;
				}
			});
		</script>

		<!--end::Fonts -->

		<!--begin::Page Vendors Styles(used by this page) -->
		<link href="<?= base_url('assets/vendors/custom/fullcalendar/fullcalendar.bundle.css');?>" rel="stylesheet" type="text/css" />

		<!--end::Page Vendors Styles -->

		<!--begin:: Global Mandatory Vendors -->
		<link href="<?= base_url('assets/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css');?>" rel="stylesheet" type="text/css" />

		<!--end:: Global Mandatory Vendors -->

		<!--begin:: Global Optional Vendors -->
		<link href="<?= base_url('assets/vendors/general/tether/dist/css/tether.css');?>" rel="stylesheet" type="text/css" />
		<link href="<?= base_url('assets/vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css');?>" rel="stylesheet" type="text/css" />
		<link href="<?= base_url('assets/vendors/general/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css');?>" rel="stylesheet" type="text/css" />
		<link href="<?= base_url('assets/vendors/general/bootstrap-timepicker/css/bootstrap-timepicker.css');?>" rel="stylesheet" type="text/css" />
		<link href="<?= base_url('assets/vendors/general/bootstrap-daterangepicker/daterangepicker.css');?>" rel="stylesheet" type="text/css" />
		<link href="<?= base_url('assets/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css');?>" rel="stylesheet" type="text/css" />
		<link href="<?= base_url('assets/vendors/general/bootstrap-select/dist/css/bootstrap-select.css');?>" rel="stylesheet" type="text/css" />
		<link href="<?= base_url('assets/vendors/general/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css');?>" rel="stylesheet" type="text/css" />
		<link href="<?= base_url('assets/vendors/general/select2/dist/css/select2.css');?>" rel="stylesheet" type="text/css" />
		<link href="<?= base_url('assets/vendors/general/ion-rangeslider/css/ion.rangeSlider.css');?>" rel="stylesheet" type="text/css" />
		<link href="<?= base_url('assets/vendors/general/nouislider/distribute/nouislider.css');?>" rel="stylesheet" type="text/css" />
		<link href="<?= base_url('assets/vendors/general/owl.carousel/dist/assets/owl.carousel.css');?>" rel="stylesheet" type="text/css" />
		<link href="<?= base_url('assets/vendors/general/owl.carousel/dist/assets/owl.theme.default.css');?>" rel="stylesheet" type="text/css" />
		<link href="<?= base_url('assets/vendors/general/dropzone/dist/dropzone.css');?>" rel="stylesheet" type="text/css" />
		<link href="<?= base_url('assets/vendors/general/summernote/dist/summernote.css');?>" rel="stylesheet" type="text/css" />
		<link href="<?= base_url('assets/vendors/general/bootstrap-markdown/css/bootstrap-markdown.min.css');?>" rel="stylesheet" type="text/css" />
		<link href="<?= base_url('assets/vendors/general/animate.css/animate.css');?>" rel="stylesheet" type="text/css" />
		<link href="<?= base_url('assets/vendors/general/toastr/build/toastr.css');?>" rel="stylesheet" type="text/css" />
		<link href="<?= base_url('assets/vendors/general/morris.js/morris.css');?>" rel="stylesheet" type="text/css" />
		<link href="<?= base_url('assets/vendors/general/sweetalert2/dist/sweetalert2.css');?>" rel="stylesheet" type="text/css" />
		<link href="<?= base_url('assets/vendors/general/socicon/css/socicon.css');?>" rel="stylesheet" type="text/css" />
		<link href="<?= base_url('assets/vendors/custom/vendors/line-awesome/css/line-awesome.css');?>" rel="stylesheet" type="text/css" />
		<link href="<?= base_url('assets/vendors/custom/vendors/flaticon/flaticon.css');?>" rel="stylesheet" type="text/css" />
		<link href="<?= base_url('assets/vendors/custom/vendors/flaticon2/flaticon.css');?>" rel="stylesheet" type="text/css" />
		<link href="<?= base_url('assets/vendors/general/@fortawesome/fontawesome-free/css/all.min.css');?>" rel="stylesheet" type="text/css" />

		<!--end:: Global Optional Vendors -->

		<!--begin::Global Theme Styles(used by all pages) -->
		<link href="<?= base_url('assets/css/demo12/style.bundle.css');?>" rel="stylesheet" type="text/css" />

		<!--end::Global Theme Styles -->

		<!--begin::Layout Skins(used by all pages) -->

		<!--end::Layout Skins -->
		<link rel="shortcut icon" href="<?= base_url('assets/media/logos/favicon.ico');?>" />
	</head>

	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading">

		<!-- begin:: Page -->

		<!-- begin:: Header Mobile -->
		<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
			<div class="kt-header-mobile__logo">
				<a href="demo12/index.html">
					<img alt="Logo" src="<?= base_url('assets/media/logos/logo-12.png');?>" />
				</a>
			</div>
			<div class="kt-header-mobile__toolbar">
				<button class="kt-header-mobile__toolbar-toggler kt-header-mobile__toolbar-toggler--left" id="kt_aside_mobile_toggler"><span></span></button>
				<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
				<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
			</div>
		</div>

		<!-- end:: Header Mobile -->
		<div class="kt-grid kt-grid--hor kt-grid--root">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

				<!-- begin:: Aside -->
				<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
				<div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">

					<!-- begin:: Aside -->
					<div class="kt-aside__brand kt-grid__item " id="kt_aside_brand">
						<div class="kt-aside__brand-logo">
							<a href="demo12/index.html">
								<img height = "40px" alt="Logo" src="<?= base_url('assets/logojakarta.png');?>">
							</a>
						</div>
						 
						 <a class="kt-aside_title"><span>Jakarta Satu</span></a>
						
						<div class="kt-aside__brand-tools">
							<button class="kt-aside__brand-aside-toggler" id="kt_aside_toggler"><span></span></button>
						</div>
					</div>

					<!-- end:: Aside -->

					<!-- begin:: Aside Menu -->
					<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
						<div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1">
							<ul class="kt-menu__nav ">
								<!-- <li class="kt-menu__section kt-menu__section--first">
									<h4 class="kt-menu__section-text">Departments</h4>
									<i class="kt-menu__section-icon flaticon-more-v2"></i>
								</li> -->
								<li class="kt-menu__item kt-menu__item--active" aria-haspopup="true"><a href="<?= base_url('dashboard');?>" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-suitcase"></i><span class="kt-menu__link-text">Halaman Utama</span></a></li>
								<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('input');?>" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-suitcase"></i><span class="kt-menu__link-text">Ajukan Permohonan</span></a></li>
								<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon flaticon-layers"></i><span class="kt-menu__link-text">Status Permohonan</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
									<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
										<ul class="kt-menu__subnav">
											<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('statusditolak');?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Ditolak</span></a></li>
											<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('statusdisetujui');?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Disetujui</span></a></li>
											<li class="kt-menu__item " aria-haspopup="true"><a href="<?= base_url('statusdiproses');?>" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Diproses</span></a></li>
										</ul>
									</div>
								</li>
							</ul>
						</div>
					</div>

					<!-- end:: Aside Menu -->
				</div>

				<!-- end:: Aside -->
				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
					<!-- begin:: Header -->
					<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">
						<!-- begin: Header Menu -->
						<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
						<div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
							<!-- <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-default ">
								<ul class="kt-menu__nav ">
									<li class="kt-menu__item  kt-menu__item--active " aria-haspopup="true"><a href="demo12/index.html" class="kt-menu__link "><span class="kt-menu__link-text">Application</span></a></li>
								</ul>
							</div> -->
						</div>
						<!-- end: Header Menu -->

						<!-- begin:: Header Topbar -->
						<div class="kt-header__topbar">

							<!--begin: Search -->
							<div class="kt-header__topbar-item kt-header__topbar-item--search dropdown" id="kt_quick_search_toggle">
								<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
									<span class="kt-header__topbar-icon"><i class="flaticon2-search-1"></i></span>
								</div>
								<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-top-unround dropdown-menu-anim dropdown-menu-lg">
									<div class="kt-quick-search kt-quick-search--inline" id="kt_quick_search_inline">
										<form method="get" class="kt-quick-search__form">
											<div class="input-group">
												<div class="input-group-prepend"><span class="input-group-text"><i class="flaticon2-search-1"></i></span></div>
												<input type="text" class="form-control kt-quick-search__input" placeholder="Search...">
												<div class="input-group-append"><span class="input-group-text"><i class="la la-close kt-quick-search__close"></i></span></div>
											</div>
										</form>
										<div class="kt-quick-search__wrapper kt-scroll" data-scroll="true" data-height="300" data-mobile-height="200">
										</div>
									</div>
								</div>
							</div>

							<!--end: Search -->





							<!--begin: User Bar -->
							<div class="kt-header__topbar-item kt-header__topbar-item--user">
								<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
									<div class="kt-header__topbar-user">
										<span class="kt-hidden kt-header__topbar-welcome kt-hidden-mobile">Hi,</span>
										<span class="kt-hidden kt-header__topbar-username kt-hidden-mobile">Sean</span>
										<img alt="Pic" class="kt-radius-100" src="<?= base_url('assets/img/img_avatar3.png');?>" />

										<!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
										<span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold kt-hidden">S</span>
									</div>
								</div>
								<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">

									<!--begin: Head -->
									<div class="kt-user-card kt-user-card--skin-light kt-notification-item-padding-x">
										<div class="kt-user-card__avatar">
											<img class="kt-hidden-" alt="Pic" src="<?= base_url('assets/img/img_avatar3.png');?>" />

											<!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
											<span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold kt-hidden">S</span>
										</div>
										<div class="kt-user-card__name">
											Login Sebagai :
											<p style="margin-text:center"><?php echo $this->session->userdata('unm'); ?></p>
											  
												
										</div>
										<!--<div class="kt-user-card__badge">
											<span class="btn btn-label-primary btn-sm btn-bold btn-font-md">23 messages</span>
										</div>-->
									</div>
                                    
									<!--end: Head -->

									<!--begin: Navigation -->
									<div class="kt-notification">

										<div class="kt-notification__custom kt-space-between">
											<a href="<?= base_url('login/logout');?>"  target="_blank" class="btn btn-label btn-label-brand btn-sm btn-bold">KELUAR</a>
											<!--<a href="demo12/custom/user/login-v2.html" target="_blank" class="btn btn-clean btn-sm btn-bold">Upgrade Plan</a>-->
										</div>
									</div>

									<!--end: Navigation -->
								</div>
							</div>

							<!--end: User Bar -->
						</div>

						<!-- end:: Header Topbar -->
					</div>

					<!-- end:: Header -->
					<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
