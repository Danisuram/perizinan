<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

Class Gate_keeper {

    public function __construct() {
        $this->obj =& get_instance();
    }

    public function is_logged_in() {

        if ($this->obj->session) {

            if ($this->obj->session->userdata('log_userid')) {
                return TRUE;

            }else{
                    return FALSE;
                }
        }else{
                return FALSE;
            }
    }

    private function get_user_id() {
        if ($this->is_logged_in()) {
            return $this->obj->session->userdata('uid');
        }
    }

    private function get_user_name() {
        if ($this->is_logged_in()) {
            return $this->obj->session->userdata('username');
        }
    }

    private function get_user_level() {
        if ($this->is_logged_in()) {
            return $this->obj->session->userdata('ulevel');
        }
    }

    public function login_routine($usr_login, $password) {

        $querynya = "SELECT * FROM LOGIN WHERE USERNAME = '".$usr_login."' AND PASSWORD = '".$password."'" ;

        $query = $this->obj->db->query($querynya);

        $data = $query->result();

        if ($data) {

          $data = json_decode(json_encode($data),true);
		  
		  //print_r($data);
		   
		  // $sess_data = array(
                    //$sess_data['loggedin'] = '1';
                    //$sess_data['u_id'] = $data->u_id;
                    //$sess_data['u_name'] = $data->u_name;
                    //$sess_data['nip'] = $data->NIP;
                    //$sess_data['nama'] = $data->NAMA;
                    //$sess_data['username'] = $data->USERNAME;
                    //$sess_data['ktp'] = $data->KTP;
                    
                    //$this->sendsession($sess_data);
              //  }
			  
			  

            
            $name = $data[0]['NAMA'];
            $username = $data[0]['USERNAME'];
            $id = $data[0]['ID'];
            $credentials = array('loggedin' => '1'
                            
                                ,'unm' => $name
                                ,'username' => $username
                                ,'id'=>$id
                    );

           $this->sendsession($credentials);
			//$this->session->set_userdata($data);

        }else{
                $this->logout_routine();
                redirect('login/login_fail');
        }
    }

    public function logout_routine() {
        $credentials = array('logged_in','uid','unm','ulevel');
        $this->obj->session->unset_userdata($credentials);
        redirect('login');
    }

    public function sendsession($credentials){
      $this->obj->session->set_userdata($credentials);
      $this->ceksession();
    }

    public function ceksession(){
      echo "<pre>";
      print_r($this->obj->session->all_userdata());
      redirect('dashboard');
    }

	function insert_log($page) {
		$date 	= date('Y-m-d H:i:s');
		$ipne 	= $this->get_ip();
		$userid = $this->obj->session->userdata('uid');

		if ($userid!='') {
			$datane = array('log_userid'=>$userid,
                            'log_date'=>$date,
                            'log_visit'=>$page,
                            'log_ip'=>$ipne);
			$this->obj->db->insert('tb_logs', $datane);
		}
	}

	function get_ip() {
		if (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
            $ips = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }else{
				$ips = $_SERVER['REMOTE_ADDR'];
			}

		return $ips;
	}


	function get_menu() {}

}
?>
